<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\FooterController;
use App\Http\Controllers\RekananController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\GalleryController;
use App\Models\PaketWedding;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/', [PagesController::class, 'home'])->middleware('auth', 'verified');

Route::get('/tambah-rekanan', [PagesController::class, 'tambahRekanan'])->middleware('auth', 'verified', 'admin');
Route::post('/tambah-rekanan', [RekananController::class, 'store'])->middleware('auth', 'verified', 'admin');

Route::post('/order', [CustomerController::class, 'store']);
Route::get('/list-customer', [PagesController::class, 'listCustomer'])->middleware('auth', 'verified');

Route::get('/new-customer', [PagesController::class, 'newCustomer'])->middleware('auth', 'verified', 'admin');
Route::get('/new-customer/invoice/{id}', [PagesController::class, 'invoice'])->middleware('auth', 'verified');
Route::post('/new-customer/metode-pembayaran', [PagesController::class, 'metodePembayaran'])->middleware('auth', 'verified');
Route::get('/new-customer/edit/{id}', [PagesController::class, 'edit'])->middleware('auth', 'verified', 'admin');

Route::get('/list-rekanan', [PagesController::class, 'listRekanan'])->middleware('auth', 'verified', 'admin');
Route::delete('/list-rekanan/{id}', [RekananController::class, 'destroy'])->middleware('auth', 'verified', 'admin');

Route::get('/form', [PagesController::class, 'form'])->middleware('auth', 'verified');
Route::get('/form-guest', [PagesController::class, 'formGuest']);

Route::get('/edit-footer', [PagesController::class, 'editFooter'])->middleware('auth', 'verified');

Route::post('/list-customer/metode-pembayaran', [PagesController::class, 'metodePembayaran'])->middleware('auth', 'verified');
Route::get('/list-customer/metode-pembayaran', [PagesController::class, 'metodePembayaran'])->middleware('auth', 'verified');


//action
Route::get('/delete/{id}', [CustomerController::class, 'destroy'])->middleware('auth', 'verified', 'admin');
Route::get('/detail', [PagesController::class, 'detail'])->middleware('auth', 'verified');
Route::get('/detailcust', [PagesController::class, 'detailCust'])->middleware('auth', 'verified');
Route::get('/list-customer/invoice/{id}', [PagesController::class, 'invoice'])->middleware('auth', 'verified');
Route::get('/list-customer/edit/{id}', [PagesController::class, 'edit'])->middleware('auth', 'verified', 'admin');
Route::get('/lunas/{id}', [CustomerController::class, 'lunas'])->middleware('auth', 'verified', 'admin');
Route::get('/inprogress/{id}', [CustomerController::class, 'inprogress'])->middleware('auth', 'verified', 'admin');
Route::get('/completed/{id}', [CustomerController::class, 'completed'])->middleware('auth', 'verified', 'admin');
Route::get('/revision/{id}', [CustomerController::class, 'revision'])->middleware('auth', 'verified', 'admin');
Route::post('/editData', [CustomerController::class, 'editData'])->middleware('auth', 'verified', 'admin');

Route::post('/edit-footer', [FooterController::class, 'create'])->middleware('auth', 'verified');

//gallery photo
Route::get('/list-customer/gallery/{id}', [PagesController::class, 'galleryCustomer'])->middleware('auth', 'verified');
Route::put('/list-customer/gallery', [GalleryController::class, 'update'])->middleware('auth', 'verified');
Route::post('/list-customer/add-photo-gallery', [GalleryController::class, 'update'])->middleware('auth', 'verified');

// Lite
Route::get('/form-lite', [PagesController::class, 'formLite']);
Route::get('/premium-update', [PagesController::class, 'premium_update']);





Route::get('/dashboard', function () {
    // return view('dashboard');
    for ($i = 1; $i < 51; $i++) {
        PaketWedding::create([
            'jenis_paket' => 'Lite',
            'demo_template' => 'demo ' . $i
        ]);
    }
})->middleware(['auth', 'verified'])->name('dashboard');

require __DIR__ . '/auth.php';
