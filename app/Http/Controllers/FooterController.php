<?php

namespace App\Http\Controllers;

use App\Models\Footer;
use Illuminate\Http\Request;

class FooterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->email == null){
            $email = Footer::find(auth()->user()->id)->email;
        }else{
            $email = $request->email;
        }

        if($request->instagram == null){
            $instagram = Footer::find(auth()->user()->id)->instagram;
        }else{
            $instagram = $request->instagram;
        }

        if($request->whatsapp == null){
            $whatsapp = Footer::find(auth()->user()->id)->whatsapp;
        }else{
            $whatsapp = $request->whatsapp;
        }

        if($request->about_us == null){
            $about_us = Footer::find(auth()->user()->id)->about_us;
        }else{
            $about_us = $request->about_us;
        }

        if(Footer::find(auth()->user()->id) == null){
            Footer::create([
                'email' => $email,
                'instagram' => $instagram,
                'whatsapp' => $whatsapp,
                'about_us' => $about_us,
                'user_id' => auth()->user()->id
            ]);
        }else{
            Footer::find(auth()->user()->id)->update([
                'email' => $email,
                'instagram' => $instagram,
                'whatsapp' => $whatsapp,
                'about_us' => $about_us,
                'user_id' => auth()->user()->id
            ]);
        }

        return redirect('https://api.whatsapp.com/send?phone=6281234567889&text=Halo%20admin%20INV,%20saya%20'.auth()->user()->name.'.%0Asaya%20ingin%20mengganti%20informasi%20Landing%20Page.%0A%0A*email%20:*%20'.$email.',%0A*whatsapp%20:*%20'.$whatsapp.',%0A*instagram%20:*%20'.$instagram.',%0A*about%20us%20:*%20'.$about_us.'%0A%0AUntuk%20Landing%20Page%20'.auth()->user()->landing_page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Footer  $footer
     * @return \Illuminate\Http\Response
     */
    public function show(Footer $footer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Footer  $footer
     * @return \Illuminate\Http\Response
     */
    public function edit(Footer $footer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Footer  $footer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Footer $footer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Footer  $footer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Footer $footer)
    {
        //
    }
}
