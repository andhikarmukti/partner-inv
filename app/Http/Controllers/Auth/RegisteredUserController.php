<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Rekanan;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use App\Providers\RouteServiceProvider;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $a = request()->a;
        return view('auth.register', [
            'a' => $a
        ]);
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        if ($request->author == null) {
            $author = 'inv';
        }else{
            $author = $request->author;
        }
        
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'nama_usaha' => ['required', 'string', 'max:255'],
            'jenis_usaha' => ['required', 'string', 'max:255'],
            'domisili' => ['required', 'string', 'max:255'],
            'url' => ['required', 'string', 'max:255'],
            'sub_domain' => ['required', 'string', 'max:255'],
            'status' => ['required', 'string', 'max:255'],
            'logo' => 'required|image|file|max:1024',
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $subdomain = 'https://' . $request->sub_domain;

        if ($request->file('logo')) {
            $logo = $request->file('logo')->store('img/rekanan');
        } else {
            $logo = null;
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'sub_domain' => $subdomain,
            'url' => $request->url,
            'landing_page' => $subdomain . '.' . $request->url,
            'logo' => $logo,
            'password' => Hash::make($request->password),
        ]);

        Rekanan::create([
            'name' => $request->name,
            'nama_usaha' => $request->nama_usaha,
            'jenis_usaha' => $request->jenis_usaha,
            'domisili' => $request->domisili,
            'email' => $request->email,
            'url_landing_page' => $request->url,
            'sub_domain' => $request->sub_domain,
            'logo' => $logo,
            'status' => $request->status,
            'author' => $author,
            $request->socialMedia1 => $request->sosmed1,
            $request->socialMedia2 => $request->sosmed2,
            $request->socialMedia3 => $request->sosmed3
        ]);
        // dd($request->email);

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }
}
