<?php

namespace App\Http\Controllers;

use App\Models\Rekanan;
use Illuminate\Http\Request;

class RekananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'tanggal_join' => 'required',
            'nama_rekanan' => 'required',
            'nama_usaha' => 'required',
            'jenis_usaha' => 'required',
            'domisili' => 'required',
            'url_landing_page' => 'required',
            'sub_domain' => 'required',
            'status' => 'required',
            'author' => 'required'
        ]);


        $logo = $request->logo;
        $nama = $request->logo->getClientOriginalName();
        $logo->move(public_path() . '/img/rekanan', $nama);

        Rekanan::create([
            'tanggal_join' => $request->tanggal_join,
            'nama_rekanan' => $request->nama_rekanan,
            'nama_usaha' => $request->nama_usaha,
            'jenis_usaha' => $request->jenis_usaha,
            'domisili' => $request->domisili,
            'url_landing_page' => $request->url_landing_page,
            'sub_domain' => $request->sub_domain,
            'logo' => $nama,
            'status' => $request->status,
            'author' => $request->author,
            $request->socialMedia1 => $request->sosmed1,
            $request->socialMedia2 => $request->sosmed2,
            $request->socialMedia3 => $request->sosmed3
        ]);


        return back()->with('berhasilTambahData', 'Data berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Rekanan  $rekanan
     * @return \Illuminate\Http\Response
     */
    public function show(Rekanan $rekanan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Rekanan  $rekanan
     * @return \Illuminate\Http\Response
     */
    public function edit(Rekanan $rekanan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Rekanan  $rekanan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rekanan $rekanan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rekanan  $rekanan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd(Rekanan::find($id));
        Rekanan::destroy($id);

        return back()->with('BerhasilHapusRekanan', 'Data rekanan berhasil dhapus!');
    }
}
