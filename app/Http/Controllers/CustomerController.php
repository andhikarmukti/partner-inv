<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->rekanan == null) {
            $rekanan = 'Admin';
        } else {
            $rekanan = $request->rekanan;
        }

        $validateData = $request->validate([
            //paket wedding
            'paket_wedding' => 'required',
            'demo' => 'required',
            'url_domain' => 'required',
            'sub_domain' => 'required',
            'music' => '',

            // data mempelai pria
            'nama_lengkap_pria' => 'required',
            'nama_panggilan_pria' => 'required',
            'putra_ke' => 'required',
            'instagram_pria' => 'required',
            'gambar_pria' => 'required|image|file|max:1024',

            // data mempelai wanita
            'nama_lengkap_wanita' => 'required',
            'nama_panggilan_wanita' => 'required',
            'putri_ke' => 'required',
            'instagram_wanita' => 'required',
            'gambar_wanita' => 'required|image|file|max:1024',

            // data orang tua pria
            'ayah_pria' => 'required',
            'ibu_pria' => 'required',

            // data orang tua wanita
            'ayah_wanita' => 'required',
            'ibu_wanita' => 'required',

            //detail acara
            'tanggal_akad' => 'required',
            'jam_akad' => 'required',
            'tanggal_resepsi' => 'required',
            'jam_resepsi' => 'required',
            'alamat_akad' => 'required',
            'alamat_resepsi' => 'required',
            'quotes' => '',
            'catatan_lain' => '',

            //rekanan
            // 'rekanan' => 'required'

        ]);

        if ($request->file('gambar_pria')) {
            $validateData['gambar_pria'] = $request->file('gambar_pria')->store('img/customer');
        }
        if ($request->file('gambar_wanita')) {
            $validateData['gambar_wanita'] = $request->file('gambar_wanita')->store('img/customer');
        }

        if ($request->paket_wedding == 'Basic') {
            $validateData['harga_paket'] = 99000;
        } elseif ($request->paket_wedding == 'Premium') {
            $validateData['harga_paket'] = 149000;
        } else {
            $validateData['harga_paket'] = 499000;
        }

        $tahun = Carbon::now()->format('Y');
        $bulan = Carbon::now()->format('m');
        $tanggal = Carbon::now()->format('d');

        $nomor_order = DB::table('customers')
            ->where('created_at', 'like', '%' . $tahun . '%')
            ->where('created_at', 'like', '%' . $bulan . '%')->count();
        $invID = str_pad($nomor_order + 1, 3, '0', STR_PAD_LEFT);

        $no_invoice = $tahun . '/' . 'INV-CO' . '/' . $bulan . $tanggal . $invID;

        $validateData['rekanan'] = $rekanan;
        $validateData['no_invoice'] = $no_invoice;
        $validateData['profit'] = $validateData['harga_paket'] * 0.3;

        //mendaptkan value deadline 1 hari
        $today = Carbon::now();
        $validateData['deadline'] = $today->addDays(1);

        Customer::create($validateData);

        $request->validate([
            'photo_1' => 'image|file|max:1080',
            'photo_2' => 'image|file|max:1080',
            'photo_3' => 'image|file|max:1080',
            'photo_4' => 'image|file|max:1080',
            'photo_5' => 'image|file|max:1080',
            'photo_6' => 'image|file|max:1080',
            'photo_7' => 'image|file|max:1080',
            'photo_8' => 'image|file|max:1080',
            'photo_9' => 'image|file|max:1080',
            'photo_10' => 'image|file|max:1080',
        ]);

        if ($request->file('photo_1')) {
            $validateData['photo_1'] = $request->file('photo_1')->store('img/customer');
        } else {
            $validateData['photo_1'] = null;
        }

        if ($request->file('photo_2')) {
            $validateData['photo_2'] = $request->file('photo_2')->store('img/customer');
        } else {
            $validateData['photo_2'] = null;
        }

        if ($request->file('photo_3')) {
            $validateData['photo_3'] = $request->file('photo_3')->store('img/customer');
        } else {
            $validateData['photo_3'] = null;
        }

        if ($request->file('photo_4')) {
            $validateData['photo_4'] = $request->file('photo_4')->store('img/customer');
        } else {
            $validateData['photo_4'] = null;
        }

        if ($request->file('photo_5')) {
            $validateData['photo_5'] = $request->file('photo_5')->store('img/customer');
        } else {
            $validateData['photo_5'] = null;
        }

        if ($request->file('photo_6')) {
            $validateData['photo_6'] = $request->file('photo_6')->store('img/customer');
        } else {
            $validateData['photo_6'] = null;
        }

        if ($request->file('photo_7')) {
            $validateData['photo_7'] = $request->file('photo_7')->store('img/customer');
        } else {
            $validateData['photo_7'] = null;
        }

        if ($request->file('photo_8')) {
            $validateData['photo_8'] = $request->file('photo_8')->store('img/customer');
        } else {
            $validateData['photo_8'] = null;
        }

        if ($request->file('photo_9')) {
            $validateData['photo_9'] = $request->file('photo_9')->store('img/customer');
        } else {
            $validateData['photo_9'] = null;
        }

        if ($request->file('photo_10')) {
            $validateData['photo_10'] = $request->file('photo_10')->store('img/customer');
        } else {
            $validateData['photo_10'] = null;
        }

        Gallery::create([
            'customer_invoice' => $no_invoice,
            'photo_1' => $validateData['photo_1'],
            'photo_2' => $validateData['photo_2'],
            'photo_3' => $validateData['photo_3'],
            'photo_4' => $validateData['photo_4'],
            'photo_5' => $validateData['photo_5'],
            'photo_6' => $validateData['photo_6'],
            'photo_7' => $validateData['photo_7'],
            'photo_8' => $validateData['photo_8'],
            'photo_9' => $validateData['photo_9'],
            'photo_10' => $validateData['photo_10']
        ]);

        $lastData = Customer::all()->last();
        // dd($lastData->created_at);


        return back()->with('berhasilTambahData', 'Data berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */

    public function editData(Request $request)
    {
        $validateData = $request->validate([
            // paket wedding
            'paket_wedding' => 'required',
            'demo' => 'required',
            'url_domain' => 'required',
            'sub_domain' => 'required',
            'music' => '',

            // data mempelai pria
            'nama_lengkap_pria' => 'required',
            'nama_panggilan_pria' => 'required',
            'putra_ke' => 'required',
            'instagram_pria' => 'required',
            'gambar_pria' => 'image|file|max:1024',

            // data mempelai wanita
            'nama_lengkap_wanita' => 'required',
            'nama_panggilan_wanita' => 'required',
            'putri_ke' => 'required',
            'instagram_wanita' => 'required',
            'gambar_wanita' => 'image|file|max:1024',

            // data orang tua pria
            'ayah_pria' => 'required',
            'ibu_pria' => 'required',

            // data orang tua wanita
            'ayah_wanita' => 'required',
            'ibu_wanita' => 'required',

            //detail acara
            'tanggal_akad' => 'required',
            'jam_akad' => 'required',
            'tanggal_resepsi' => 'required',
            'jam_resepsi' => 'required',
            'alamat_akad' => 'required',
            'alamat_resepsi' => 'required',
            'quotes' => '',
            'catatan_lain' => '',

            //rekanan
            'rekanan' => 'required'
        ]);

        // dd($validateData);
        if ($request->file('gambar_pria')) {
            //jika ada gambar lama, maka gambar lama dihapus dulu
            if ($request->gambar_pria_lama) {
                Storage::delete($request->gambar_pria_lama);
            }
            //baru diisi dengan gambar baru yang dipilih
            $validateData['gambar_pria'] = $request->file('gambar_pria')->store('img/customer');
        }

        if ($request->file('gambar_wanita')) {
            //jika ada gambar lama, maka gambar lama dihapus dulu
            if ($request->gambar_wanita_lama) {
                Storage::delete($request->gambar_wanita_lama);
            }
            //baru diisi dengan gambar baru yang dipilih
            $validateData['gambar_wanita'] = $request->file('gambar_wanita')->store('img/customer');
        }

        if ($request->paket_wedding == 'Basic') {
            $validateData['harga_paket'] = 99000;
        } elseif ($request->paket_wedding == 'Premium') {
            $validateData['harga_paket'] = 149000;
        } else {
            $validateData['harga_paket'] = 499000;
        }

        //Jika orderan dari admin, tidak mendapatkan discount 30% untuk hitungan profitnya
        if ($validateData['rekanan'] == 'Admin') {
            $validateData['profit'] = $validateData['harga_paket'] * 1;
        } else {
            $validateData['profit'] = $validateData['harga_paket'] * 0.3;
        }
        $newCustomer = Customer::where('pembayaran', 'belum dibayar')->get()->count();

        Customer::where('id', $request->id)->update($validateData);

        $data = Customer::all();
        return back()->with('berhasilEdit', 'Data berhasil diubah');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Customer::destroy($id);

        return back()->with('berhasilHapus', 'Data berhasil dihapus');
    }

    public function lunas($id)
    {
        Customer::find($id)->update([
            'pembayaran' => 'lunas'
        ]);

        return back()->with('BerhasilLunas', 'Pembayaran dengan order id ' . $id . ' telah lunas');
    }

    public function inprogress($id)
    {
        Customer::find($id)->update([
            'pengerjaan' => 'inprogress'
        ]);

        return back()->with('BerhasilInprogress', 'Pengerjaan landing page dengan order id ' . $id . ' sedang dalam proses pengerjaan');
    }

    public function completed($id)
    {
        Customer::find($id)->update([
            'pengerjaan' => 'completed'
        ]);

        return back()->with('BerhasilCompleted', 'Pengerjaan landing page dengan order id ' . $id . ' teleh selesai');
    }

    public function revision($id)
    {
        Customer::find($id)->update([
            'pengerjaan' => 'revision'
        ]);

        return back()->with('BerhasilRevision', 'Pengerjaan landing page dengan order id ' . $id . ' sedang dalam tahap revisi');
    }
}
