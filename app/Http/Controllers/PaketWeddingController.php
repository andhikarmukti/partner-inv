<?php

namespace App\Http\Controllers;

use App\Models\PaketWedding;
use Illuminate\Http\Request;

class PaketWeddingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PaketWedding  $paketWedding
     * @return \Illuminate\Http\Response
     */
    public function show(PaketWedding $paketWedding)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PaketWedding  $paketWedding
     * @return \Illuminate\Http\Response
     */
    public function edit(PaketWedding $paketWedding)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PaketWedding  $paketWedding
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaketWedding $paketWedding)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PaketWedding  $paketWedding
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaketWedding $paketWedding)
    {
        //
    }
}
