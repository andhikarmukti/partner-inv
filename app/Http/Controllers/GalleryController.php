<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $no_invoice = Customer::where('id', $request->id)->pluck('no_invoice')->toArray();
        if($request->file('photo_1')){
            if ($request->photo_1_lama) {
                Storage::delete($request->photo_1_lama);
            }
            $photo_1 = $request->file('photo_1')->store('img/customer');
        }
        if($request->file('photo_2')){
            if ($request->photo_2_lama) {
                Storage::delete($request->photo_2_lama);
            }
            $photo_2 = $request->file('photo_2')->store('img/customer');
        }
        if($request->file('photo_3')){
            if ($request->photo_3_lama) {
                Storage::delete($request->photo_3_lama);
            }
            $photo_3 = $request->file('photo_3')->store('img/customer');
        }
        if($request->file('photo_4')){
            if ($request->photo_4_lama) {
                Storage::delete($request->photo_4_lama);
            }
            $photo_4 = $request->file('photo_4')->store('img/customer');
        }
        if($request->file('photo_5')){
            if ($request->photo_5_lama) {
                Storage::delete($request->photo_5_lama);
            }
            $photo_5 = $request->file('photo_5')->store('img/customer');
        }
        if($request->file('photo_6')){
            if ($request->photo_6_lama) {
                Storage::delete($request->photo_6_lama);
            }
            $photo_6 = $request->file('photo_6')->store('img/customer');
        }
        if($request->file('photo_7')){
            if ($request->photo_7_lama) {
                Storage::delete($request->photo_7_lama);
            }
            $photo_7 = $request->file('photo_7')->store('img/customer');
        }
        if($request->file('photo_8')){
            if ($request->photo_8_lama) {
                Storage::delete($request->photo_8_lama);
            }
            $photo_8 = $request->file('photo_8')->store('img/customer');
        }
        if($request->file('photo_9')){
            if ($request->photo_9_lama) {
                Storage::delete($request->photo_9_lama);
            }
            $photo_9 = $request->file('photo_9')->store('img/customer');
        }
        if($request->file('photo_10')){
            if ($request->photo_10_lama) {
                Storage::delete($request->photo_10_lama);
            }
            $photo_10 = $request->file('photo_10')->store('img/customer');
        }
        
        if($request->photo_1 == null){
            $photo_1 = $request->photo_1_lama;
        }
        if($request->photo_2 == null){
            $photo_2 = $request->photo_2_lama;
        }
        if($request->photo_3 == null){
            $photo_3 = $request->photo_3_lama;
        }
        if($request->photo_4 == null){
            $photo_4 = $request->photo_4_lama;
        }
        if($request->photo_5 == null){
            $photo_5 = $request->photo_5_lama;
        }
        if($request->photo_6 == null){
            $photo_6 = $request->photo_6_lama;
        }
        if($request->photo_7 == null){
            $photo_7 = $request->photo_7_lama;
        }
        if($request->photo_8 == null){
            $photo_8 = $request->photo_8_lama;
        }
        if($request->photo_9 == null){
            $photo_9 = $request->photo_9_lama;
        }
        if($request->photo_10 == null){
            $photo_10 = $request->photo_10_lama;
        }
        // dd([
        //     $photo_4
        // ]);
        $cekInvoice = Gallery::where('customer_invoice', $no_invoice)->get()->count();
        $new_no_invoice = Customer::where('id', $request->id)->get()->pluck('no_invoice')->toArray();
        if($cekInvoice == 0){
            Gallery::create([
                'customer_invoice' => $new_no_invoice[0],
                'photo_1' => $photo_1,
                'photo_2' => $photo_2,
                'photo_3' => $photo_3,
                'photo_4' => $photo_4,
                'photo_5' => $photo_5,
                'photo_6' => $photo_6,
                'photo_7' => $photo_7,
                'photo_8' => $photo_8,
                'photo_9' => $photo_9,
                'photo_10' => $photo_10
            ]);
        }
        Gallery::where('customer_invoice', $no_invoice)->update([
            'photo_1' => $photo_1,
            'photo_2' => $photo_2,
            'photo_3' => $photo_3,
            'photo_4' => $photo_4,
            'photo_5' => $photo_5,
            'photo_6' => $photo_6,
            'photo_7' => $photo_7,
            'photo_8' => $photo_8,
            'photo_9' => $photo_9,
            'photo_10' => $photo_10
        ]);
        
        return back()->with('berhasil', 'Berhasil melakukan perubahan gallery');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gallery $gallery)
    {
        //
    }
}
