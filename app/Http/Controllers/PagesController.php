<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Footer;
use App\Models\Gallery;
use App\Models\Rekanan;
use App\Models\Customer;
use App\Models\PaketWedding;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Date;

class PagesController extends Controller
{
    public function home()
    {
        //update status pembayaran jadi expired kalau sudah lewat now()
        $customerAll = Customer::all();
        foreach ($customerAll as $customer) {
            for ($i = 1; $i < Customer::all()->count(); $i++) {
                if ($customer->pembayaran !== 'lunas') {
                    if ($customer->deadline < Carbon::now()) {
                        Customer::find($customer->id)->update([
                            'pembayaran' => 'Expired'
                        ]);
                    }
                }
            }
        }

        $thisMonth = Carbon::now()->format('m');
        $thisYear = Carbon::now()->format('Y');

        $jumlahOrderJanuari = DB::table('customers')->whereMonth('created_at', '01')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->get()->count();
        $jumlahOrderFebruari = DB::table('customers')->whereMonth('created_at', '02')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->get()->count();
        $jumlahOrderMaret = DB::table('customers')->whereMonth('created_at', '03')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->get()->count();
        $jumlahOrderApril = DB::table('customers')->whereMonth('created_at', '04')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->get()->count();
        $jumlahOrderMei = DB::table('customers')->whereMonth('created_at', '05')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->get()->count();
        $jumlahOrderJuni = DB::table('customers')->whereMonth('created_at', '06')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->get()->count();
        $jumlahOrderJuli = DB::table('customers')->whereMonth('created_at', '07')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->get()->count();
        $jumlahOrderAgustus = DB::table('customers')->whereMonth('created_at', '08')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->get()->count();
        $jumlahOrderSeptember = DB::table('customers')->whereMonth('created_at', '09')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->get()->count();
        $jumlahOrderOktober = DB::table('customers')->whereMonth('created_at', '10')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->get()->count();
        $jumlahOrderNovember = DB::table('customers')->whereMonth('created_at', '11')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->get()->count();
        $jumlahOrderDesember = DB::table('customers')->whereMonth('created_at', '12')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->get()->count();
        $jumlahOrderBulanIni = DB::table('customers')->whereMonth('created_at', $thisMonth)
            ->whereYear('created_at', $thisYear)
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->get()->count();

        $profitJanuari = DB::table('customers')->whereMonth('created_at', '01')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->pluck('profit')->sum();
        $profitFebruari = DB::table('customers')->whereMonth('created_at', '02')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->pluck('profit')->sum();
        $profitMaret = DB::table('customers')->whereMonth('created_at', '03')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->pluck('profit')->sum();
        $profitApril = DB::table('customers')->whereMonth('created_at', '04')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->pluck('profit')->sum();
        $profitMei = DB::table('customers')->whereMonth('created_at', '05')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->pluck('profit')->sum();
        $profitJuni = DB::table('customers')->whereMonth('created_at', '06')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->pluck('profit')->sum();
        $profitJuli = DB::table('customers')->whereMonth('created_at', '07')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->pluck('profit')->sum();
        $profitAgustus = DB::table('customers')->whereMonth('created_at', '08')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->pluck('profit')->sum();
        $profitSeptember = DB::table('customers')->whereMonth('created_at', '09')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->pluck('profit')->sum();
        $profitOktober = DB::table('customers')->whereMonth('created_at', '10')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->pluck('profit')->sum();
        $profitNovember = DB::table('customers')->whereMonth('created_at', '11')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->pluck('profit')->sum();
        $profitDesember = DB::table('customers')->whereMonth('created_at', '12')
            ->whereYear('created_at', '2021')
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->pluck('profit')->sum();
        $profitBulanIni = DB::table('customers')->whereMonth('created_at', $thisMonth)
            ->whereYear('created_at', $thisYear)
            ->where('rekanan', auth()->user()->name)
            ->where('pembayaran', 'lunas')
            ->pluck('profit')->sum();

        if (auth()->user()->role == 'admin') {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')->get()->count();
        } else {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')
                ->where('rekanan', auth()->user()->name)
                ->get()->count();
        }

        //sapaan dinamis (selamat pagi, siang, sore dan malam)
        $thisTime = Carbon::now()->format('H:i:s');
        if (strtotime($thisTime) >= strtotime("19:00:00") || strtotime($thisTime) < strtotime("05:00:00")) {
            $time = "Selamat malam";
        } else if (strtotime($thisTime) >= strtotime("05:00:00") && strtotime($thisTime) < strtotime("11:00:00")) {
            $time = "Selamat pagi";
        } else if (strtotime($thisTime) >= strtotime("11:00:00") && strtotime($thisTime) < strtotime("15:00:00")) {
            $time = "Selamat siang";
        } else if (strtotime($thisTime) >= strtotime("15:00:00") && strtotime($thisTime) < strtotime("19:00:00")) {
            $time = "Selamat sore";
        }

        return view('home', [
            'title' => 'Home',
            'jumlahOrderJanuari' => $jumlahOrderJanuari,
            'jumlahOrderFebruari' => $jumlahOrderFebruari,
            'jumlahOrderMaret' => $jumlahOrderMaret,
            'jumlahOrderApril' => $jumlahOrderApril,
            'jumlahOrderMei' => $jumlahOrderMei,
            'jumlahOrderJuni' => $jumlahOrderJuni,
            'jumlahOrderJuli' => $jumlahOrderJuli,
            'jumlahOrderAgustus' => $jumlahOrderAgustus,
            'jumlahOrderSeptember' => $jumlahOrderSeptember,
            'jumlahOrderOktober' => $jumlahOrderOktober,
            'jumlahOrderNovember' => $jumlahOrderNovember,
            'jumlahOrderDesember' => $jumlahOrderDesember,
            'jumlahOrderBulanIni' => $jumlahOrderBulanIni,
            'profitJanuari' => $profitJanuari,
            'profitFebruari' => $profitFebruari,
            'profitMaret' => $profitMaret,
            'profitApril' => $profitApril,
            'profitMei' => $profitMei,
            'profitJuni' => $profitJuni,
            'profitJuli' => $profitJuli,
            'profitAgustus' => $profitAgustus,
            'profitSeptember' => $profitSeptember,
            'profitOktober' => $profitOktober,
            'profitNovember' => $profitNovember,
            'profitDesember' => $profitDesember,
            'profitBulanIni' => $profitBulanIni,
            'newCustomer' => $newCustomer,
            'time' => $time
        ]);
    }

    public function tambahRekanan()
    {
        //update status pembayaran jadi expired kalau sudah lewat now()
        $customerAll = Customer::all();
        foreach ($customerAll as $customer) {
            for ($i = 1; $i < Customer::all()->count(); $i++) {
                if ($customer->pembayaran !== 'lunas') {
                    if ($customer->deadline < Carbon::now()) {
                        Customer::find($customer->id)->update([
                            'pembayaran' => 'Expired'
                        ]);
                    }
                }
            }
        }

        if (auth()->user()->role == 'admin') {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')->get()->count();
        } else {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')
                ->where('rekanan', auth()->user()->name)
                ->get()->count();
        }
        $a = request()->a;
        // dd($a);
        return view('customer.tambahRekanan', [
            'a' => $a,
            'title' => 'Tambah Data Rekanan',
            'newCustomer' => $newCustomer
        ]);
    }

    public function listRekanan()
    {
        //update status pembayaran jadi expired kalau sudah lewat now()
        $customerAll = Customer::all();
        foreach ($customerAll as $customer) {
            for ($i = 1; $i < Customer::all()->count(); $i++) {
                if ($customer->pembayaran !== 'lunas') {
                    if ($customer->deadline < Carbon::now()) {
                        Customer::find($customer->id)->update([
                            'pembayaran' => 'Expired'
                        ]);
                    }
                }
            }
        }

        if (auth()->user()->role == 'admin') {
            $data = Rekanan::all();
        } else {
            $data = Rekanan::where('author', auth()->user()->name);
        }

        if (auth()->user()->role == 'admin') {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')->get()->count();
        } else {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')
                ->where('rekanan', auth()->user()->name)
                ->get()->count();
        }

        if (request('keyword')) {
            $keyword = request('keyword');
            $data = Rekanan::where('id', 'like', '%' . $keyword . '%')
                ->orWhere('name', 'like', '%' . $keyword . '%')
                ->orWhere('domisili', 'like', '%' . $keyword . '%')
                ->orWhere('whatsapp', 'like', '%' . $keyword . '%')
                ->orWhere('instagram', 'like', '%' . $keyword . '%')
                ->orWhere('facebook', 'like', '%' . $keyword . '%')
                ->orWhere('twitter', 'like', '%' . $keyword . '%')
                ->orWhere('url_landing_page', 'like', '%' . $keyword . '%')
                ->orWhere('sub_domain', 'like', '%' . $keyword . '%')
                ->orWhere('author', 'like', '%' . $keyword . '%')
                ->get();
        }
        return view('customer.listRekanan', [
            'title' => 'List Rekanan',
            'data' => $data,
            'newCustomer' => $newCustomer
        ]);
    }


    public static function listCustomer()
    {
        //update status pembayaran jadi expired kalau sudah lewat now()
        $customerAll = Customer::all();
        foreach ($customerAll as $customer) {
            for ($i = 1; $i < Customer::all()->count(); $i++) {
                if ($customer->pembayaran !== 'lunas') {
                    if ($customer->deadline < Carbon::now()) {
                        Customer::find($customer->id)->update([
                            'pembayaran' => 'Expired'
                        ]);
                    }
                }
            }
        }

        if (auth()->user()->role == 'admin') {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')->get()->count();
        } else {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')
                ->where('rekanan', auth()->user()->name)
                ->get()->count();
        }
        // dd(request()->keyword);
        //search

        //menampilkan menu admin
        if (auth()->user()->role == 'admin') {
            $data = Customer::all();
            if (request()->keyword) {
                $keyword = request()->keyword;
                $data = Customer::where('paket_wedding', 'like', '%' . $keyword . '%')
                    ->orWhere('demo', 'like', '%' . $keyword . '%')
                    ->orWhere('url_domain', 'like', '%' . $keyword . '%')
                    ->orWhere('sub_domain', 'like', '%' . $keyword . '%')
                    ->orWhere('pembayaran', 'like', '%' . $keyword . '%')
                    ->orWhere('pengerjaan', 'like', '%' . $keyword . '%')
                    ->orWhere('rekanan', 'like', '%' . $keyword . '%')
                    ->get();
            }
        } else {
            $data = Customer::where('rekanan', auth()->user()->name)->orderBy('id', 'desc')->get();
            if (request()->keyword) {
                $keyword = request()->keyword;
                $data = Customer::where('paket_wedding', 'like', '%' . $keyword . '%')
                    ->orWhere('rekanan', auth()->user()->name)
                    ->orWhere('demo', 'like', '%' . $keyword . '%')
                    ->orWhere('url_domain', 'like', '%' . $keyword . '%')
                    ->orWhere('sub_domain', 'like', '%' . $keyword . '%')
                    ->orWhere('pembayaran', 'like', '%' . $keyword . '%')
                    ->orWhere('pengerjaan', 'like', '%' . $keyword . '%')
                    ->get();
            }
        }

        return view('customer.listCustomer', [
            'title' => 'List Customer',
            'data' => $data,
            'newCustomer' => $newCustomer
        ]);
    }

    public function form(Request $request)
    {
        //update status pembayaran jadi expired kalau sudah lewat now()
        $customerAll = Customer::all();
        foreach ($customerAll as $customer) {
            for ($i = 1; $i < Customer::all()->count(); $i++) {
                if ($customer->pembayaran !== 'lunas') {
                    if ($customer->deadline < Carbon::now()) {
                        Customer::find($customer->id)->update([
                            'pembayaran' => 'Expired'
                        ]);
                    }
                }
            }
        }

        if (auth()->user()->role == 'admin') {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')->get()->count();
        } else {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')
                ->where('rekanan', auth()->user()->name)
                ->get()->count();
        }

        if ($request->paket) {
            $result = PaketWedding::where('jenis_paket', $request->paket)->pluck('demo_template');

            return response()->json($result);
        }

        return view('form', [
            'title' => 'Form',
            'newCustomer' => $newCustomer
        ]);
    }

    public function formGuest(Request $request)
    {
        //update status pembayaran jadi expired kalau sudah lewat now()
        $customerAll = Customer::all();
        foreach ($customerAll as $customer) {
            for ($i = 1; $i < Customer::all()->count(); $i++) {
                if ($customer->pembayaran !== 'lunas') {
                    if ($customer->deadline < Carbon::now()) {
                        Customer::find($customer->id)->update([
                            'pembayaran' => 'Expired'
                        ]);
                    }
                }
            }
        }

        if ($request->paket) {
            $result = PaketWedding::where('jenis_paket', $request->paket)->pluck('demo_template');
            return response()->json($result);
        }

        // r adalah rekanan, untuk disembunyikan saja tujuannya
        $r = $request->r;
        return view('formGuest', [
            'title' => 'Form Guest',
            'r' => $r
        ]);
    }

    public function formLite(Request $request)
    {
        $demoWedding = PaketWedding::where('jenis_paket', 'Lite')->get();

        $r = $request->r;
        return view('formLite', [
            'title' => 'Form Guest',
            'r' => $r,
            'demoWedding' => $demoWedding
        ]);
    }

    public function newCustomer()
    {
        //update status pembayaran jadi expired kalau sudah lewat now()
        $customerAll = Customer::all();
        foreach ($customerAll as $customer) {
            for ($i = 1; $i < Customer::all()->count(); $i++) {
                if ($customer->pembayaran !== 'lunas') {
                    if ($customer->deadline < Carbon::now()) {
                        Customer::find($customer->id)->update([
                            'pembayaran' => 'Expired'
                        ]);
                    }
                }
            }
        }

        if (auth()->user()->role == 'admin') {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')->get()->count();
        } else {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')
                ->where('rekanan', auth()->user()->name)
                ->get()->count();
        }
        $data = Customer::where('pembayaran', 'Belum Dibayar')->get();
        return view('customer.newCustomer', [
            'title' => 'New Customer',
            'data' => $data,
            'newCustomer' => $newCustomer
        ]);
    }

    public function detail(Request $request)
    {
        if (auth()->user()->role == 'admin') {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')->get()->count();
        } else {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')
                ->where('rekanan', auth()->user()->name)
                ->get()->count();
        }
        $data = Customer::where('id', $request->id)->get();

        return view('customer.detail', [
            'title' => 'detail',
            'data' => $data,
            'newCustomer' => $newCustomer
        ]);
    }

    public function detailCust(Request $request)
    {
        //data pria
        $nama_lengkap_pria = Customer::where('id', $request->detail)->pluck('nama_lengkap_pria');
        $nama_panggilan_pria = Customer::where('id', $request->detail)->pluck('nama_panggilan_pria');
        $ibu_pria = Customer::where('id', $request->detail)->pluck('ibu_pria');
        $ayah_pria = Customer::where('id', $request->detail)->pluck('ayah_pria');
        $putra_ke = Customer::where('id', $request->detail)->pluck('putra_ke');
        $instagram_pria = Customer::where('id', $request->detail)->pluck('instagram_pria');
        $gambar_pria = Customer::where('id', $request->detail)->pluck('gambar_pria');

        //data wanita
        $nama_lengkap_wanita = Customer::where('id', $request->detail)->pluck('nama_lengkap_wanita');
        $nama_panggilan_wanita = Customer::where('id', $request->detail)->pluck('nama_panggilan_wanita');
        $ibu_wanita = Customer::where('id', $request->detail)->pluck('ibu_wanita');
        $ayah_wanita = Customer::where('id', $request->detail)->pluck('ayah_wanita');
        $putri_ke = Customer::where('id', $request->detail)->pluck('putri_ke');
        $instagram_wanita = Customer::where('id', $request->detail)->pluck('instagram_wanita');
        $gambar_wanita = Customer::where('id', $request->detail)->pluck('gambar_wanita');

        //data akad & resepsi
        $tanggal_akad = Customer::where('id', $request->detail)->pluck('tanggal_akad');
        $jam_akad = Customer::where('id', $request->detail)->pluck('jam_akad');
        $tanggal_resepsi = Customer::where('id', $request->detail)->pluck('tanggal_resepsi');
        $jam_resepsi = Customer::where('id', $request->detail)->pluck('jam_resepsi');
        $alamat_akad = Customer::where('id', $request->detail)->pluck('alamat_akad');

        return response()->json([
            $nama_lengkap_pria,
            $nama_panggilan_pria,
            $ibu_pria,
            $ayah_pria,
            $putra_ke,
            $instagram_pria,
            $gambar_pria,
            $nama_lengkap_wanita,
            $nama_panggilan_wanita,
            $ibu_wanita,
            $ayah_wanita,
            $putri_ke,
            $instagram_wanita,
            $tanggal_akad,
            $jam_akad,
            $tanggal_resepsi,
            $jam_resepsi,
            $alamat_akad,
            $gambar_wanita
        ]);
    }

    public function invoice($id)
    {
        if (auth()->user()->role == 'admin') {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')->get()->count();
        } else {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')
                ->where('rekanan', auth()->user()->name)
                ->get()->count();
        }
        $data = Customer::find($id);
        $discount = $data->harga_paket * 0.3;
        $no_invoice = $data->no_invoice;

        return view('customer.invoice', [
            'title' => 'Invoice',
            'data' => $data,
            'discount' => $discount,
            'no_invoice' => $no_invoice,
            'newCustomer' => $newCustomer
        ]);
    }

    public function edit($id)
    {
        if (auth()->user()->role == 'admin') {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')->get()->count();
        } else {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')
                ->where('rekanan', auth()->user()->name)
                ->get()->count();
        }
        $data = Customer::find($id);
        // dd($data);
        return view('customer.edit', [
            'title' => 'Edit',
            'data' => $data,
            'newCustomer' => $newCustomer
        ]);
    }

    public function metodePembayaran(Request $request)
    {
        if (auth()->user()->role == 'admin') {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')->get()->count();
        } else {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')
                ->where('rekanan', auth()->user()->name)
                ->get()->count();
        }
        if ($request->status_pembayaran) {
            Customer::find($request->id_customer)->update([
                'pembayaran' => 'Expired'
            ]);

            return response()->json('ok');
        }

        //jika metode pembayaran belum dipilih
        if (request()->metode_pembayaran == null) {
            return back()->with('metodePembayaranNull', 'Silahkan pilih metode pembayaran yang tersedia');
        }

        //sortir metode pembayaran yang dipilih
        // dd(request()->metode_pembayaran);
        if (request()->metode_pembayaran == 'BCA') {
            $data = [
                'atasNama' => 'Akmal Januar Pratama',
                'noRek' => '2952296907',
                'namaBank' => 'BCA'
            ];
        } elseif (request()->metode_pembayaran == 'Mandiri') {
            $data = [
                'atasNama' => 'Akmal Januar Pratama',
                'noRek' => '1400015858906',
                'namaBank' => 'Mandiri'
            ];
        } elseif (request()->metode_pembayaran == 'BNI') {
            $data = [
                'atasNama' => 'Akmal Januar Pratama',
                'noRek' => '0267396281',
                'namaBank' => 'BNI'
            ];
        } elseif (request()->metode_pembayaran == 'BRI') {
            $data = [
                'atasNama' => 'Akmal Januar Pratama',
                'noRek' => '044401021664508',
                'namaBank' => 'BRI'
            ];
        } elseif (request()->metode_pembayaran == 'OVO') {
            $data = [
                'atasNama' => 'Akmal Januar Pratama',
                'noRek' => '081213175987',
                'namaBank' => 'OVO'
            ];
        }

        $customer = Customer::all();
        foreach ($customer as $cust) {
        }

        return view('customer.metodePembayaran', [
            'title' => 'Metode Pembayaran',
            'data' => $data,
            'cust' => $cust,
            'newCustomer' => $newCustomer
        ]);
    }

    public function editFooter()
    {

        if (auth()->user()->role == 'admin') {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')->get()->count();
        } else {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')
                ->where('rekanan', auth()->user()->name)
                ->get()->count();
        }

        if (Footer::find(auth()->user()->id)) {
            $footer = Footer::find(auth()->user()->id);
        } else {
            $footer = null;
        }

        return view('editFooter', [
            'title' => 'Edit Footer',
            'newCustomer' => $newCustomer,
            'footer' => $footer
        ]);
    }

    public function galleryCustomer($id)
    {
        if (auth()->user()->role == 'admin') {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')->get()->count();
        } else {
            $newCustomer = Customer::where('pembayaran', 'belum dibayar')
                ->where('rekanan', auth()->user()->name)
                ->get()->count();
        }

        $no_invoice = Customer::where('id', $id)->pluck('no_invoice')->toArray();
        $gallery = Gallery::where('customer_invoice', $no_invoice[0])->get()->toArray();
        // dd($gallery);
        if ($gallery) {
            $gallery = $gallery;
        } else {
            $gallery = [
                [
                    'photo_1' => 'photo_1',
                    'photo_2' => 'photo_2',
                    'photo_3' => 'photo_3',
                    'photo_4' => 'photo_4',
                    'photo_5' => 'photo_5',
                    'photo_6' => 'photo_6',
                    'photo_7' => 'photo_7',
                    'photo_8' => 'photo_8',
                    'photo_9' => 'photo_9',
                    'photo_10' => 'photo_10'
                ]
            ];
        }

        return view('customer.gallery-customer', [
            'title' => 'Add Photo Gallery',
            'newCustomer' => $newCustomer,
            'gallery' => $gallery[0],
            'id' => $id
        ]);
    }

    public function premium_update()
    {
        PaketWedding::where('jenis_paket', 'premium')->delete();
        for ($i = 1; $i < 50; $i++) {
            PaketWedding::create([
                'jenis_paket' => 'premium',
                'demo_template' => 'premium ' . $i
            ]);
        }

        return 'ok';
    }
}
