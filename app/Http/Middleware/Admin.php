<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //pengecekan apabila dia belom login (guest) maka abort
        //bisa juga pake method check() tapi kasih ! di awal auth() nya
        if(auth()->guest()){
            abort(403);
            if(auth()->user()->role !== 'admin' && auth()->user()->role !== 'staff'){
                abort(403);
            }
        }
        return $next($request);
    }
}
