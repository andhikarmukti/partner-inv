{{-- <x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <!-- Name -->
            <div>
                <x-label for="name" :value="__('Name')" />

                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
            </div>

            <!-- Email Address -->
            <div class="mt-4">
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <!-- Landing Page -->
            <div class="mt-4">
                <label for="sub_domain">Subdomain</label>
                <input id="sub_domain" class="block mt-1 w-full" type="text" name="sub_domain" required>
            </div>
            <div class="mt-4">
                <label for="url">URL</label>
                <select name="url" id="url">
                    <option value="" selected disabled>Pilih URL</option>
                    <option value="momenspesial.my.id">momenspesial.my.id</option>
                    <option value="undang.pw">undang.pw</option>
                    <option value="haribahagia.pw">haribahagia.pw</option>
                </select>
            </div>

            <div class="mt-4">
                <label for="nama_usaha">Nama Usaha</label>
                <input id="nama_usaha" class="block mt-1 w-full" type="text" name="nama_usaha" required>
            </div>

            <div class="mt-4">
                <label for="jenis_usaha">Jenis Usaha</label>
                <input id="jenis_usaha" class="block mt-1 w-full" type="text" name="jenis_usaha" required>
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required autocomplete="new-password" />
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Confirm Password')" />

                <x-input id="password_confirmation" class="block mt-1 w-full"
                                type="password"
                                name="password_confirmation" required />
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-button class="ml-4">
                    {{ __('Register') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout> --}}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="shortcut icon" href="/img/Favicon Circle.png">
    <title>Registration</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <form enctype="multipart/form-data" action="{{ route('register') }}" method="post" class="mt-4">
                    @csrf
                    <div class="mb-3">
                      <label for="name" class="form-label">Nama</label>
                      <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}">
                      @error('name')
                      <div id="validationServer03Feedback" class="invalid-feedback">
                          {{ $message }}
                        </div>
                      @enderror
                    </div>

                    <div class="mb-3">
                      <label for="email" class="form-label">Email</label>
                      <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}">
                      @error('email')
                      <div id="validationServer03Feedback" class="invalid-feedback">
                          {{ $message }}
                        </div>
                      @enderror
                    </div>

                    <div class="row">
                    <div class="mb-3 col">
                      <label for="password" class="form-label">Password</label>
                      <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" value="{{ old('password') }}">
                      @error('password')
                      <div id="validationServer03Feedback" class="invalid-feedback">
                          {{ $message }}
                        </div>
                      @enderror
                    </div>
                    <div class="mb-3 col">
                      <label for="password_confirmation" class="form-label">Confirm Password</label>
                      <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" id="password_confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}">
                      @error('password_confirmation')
                      <div id="validationServer03Feedback" class="invalid-feedback">
                          {{ $message }}
                        </div>
                      @enderror
                    </div>
                    </div>

                    <hr>

                    <div class="row">
                      <div class="mb-3 col">
                        <label for="nama_usaha" class="form-label">Nama Usaha</label>
                        <input type="text" class="form-control @error('nama_usaha') is-invalid @enderror" id="nama_usaha" name="nama_usaha" value="{{ old('nama_usaha') }}">
                        @error('nama_usaha')
                        <div id="validationServer03Feedback" class="invalid-feedback">
                            {{ $message }}
                          </div>
                        @enderror
                      </div>
                      <div class="mb-3 col">
                        <label for="jenis_usaha" class="form-label">Jenis Usaha</label>
                        <input type="text" class="form-control @error('jenis_usaha') is-invalid @enderror" id="jenis_usaha" name="jenis_usaha" value="{{ old('jenis_usaha') }}">
                        @error('jenis_usaha')
                        <div id="validationServer03Feedback" class="invalid-feedback">
                            {{ $message }}
                          </div>
                        @enderror
                      </div>
                    </div>

                    <div class="row mb-3">
                      <div class="col mb-3">
                        <select name="socialMedia1" id="socialMedia1" class="mb-2">
                          <option value="whatsapp" selected>WhatsApp</option>
                        </select>
                        <input type="text" class="form-control @error('sosmed1') is-invalid @enderror" id="sosmed1" name="sosmed1" value="{{ old('sosmed1') }}">
                        @error('sosmed1')
                        <div id="validationServer03Feedback" class="invalid-feedback">
                          {{ $message }}
                        </div>
                      @enderror
                      </div>
                      <div class="col mb-3">
                        <select name="socialMedia2" id="socialMedia2" class="mb-2">
                          <option value="whatsapp">WhatsApp</option>
                          <option value="instagram" selected>Instagram</option>
                          <option value="facebook">Facebook</option>
                          <option value="twitter">Twitter</option>
                        </select>
                        <input type="text" class="form-control @error('sosmed2') is-invalid @enderror" id="whatsapp" name="sosmed2" value="{{ old('sosmed2') }}">
                      </div>
                      <div class="col mb-3">
                        <select name="socialMedia3" id="socialMedia3" class="mb-2">
                          <option value="whatsapp">WhatsApp</option>
                          <option value="instagram">Instagram</option>
                          <option value="facebook" selected>Facebook</option>
                          <option value="twitter">Twitter</option>
                        </select>
                        <input type="text" class="form-control @error('sosmed3') is-invalid @enderror" id="whatsapp" name="sosmed3" value="{{ old('sosmed3') }}">
                      </div>
                      @error('jenisUsaha')
                      <div id="validationServer03Feedback" class="invalid-feedback">
                          {{ $message }}
                        </div>
                      @enderror
                    </div>

                    <div class="mb-3">
                      <label for="domisili" class="form-label">Domisili</label>
                      <input type="text" class="form-control @error('domisili') is-invalid @enderror" id="domisili" name="domisili" value="{{ old('domisili') }}">
                      @error('domisili')
                      <div id="validationServer03Feedback" class="invalid-feedback">
                          {{ $message }}
                        </div>
                      @enderror
                    </div>
                    <hr>

                    <div class="d-flex">
                      <div class="col-5">
                        <label for="url" class="form-label">URL</label>
                        <select name="url" id="url" class="form-select @error('url') is-invalid @enderror">
                            <option value="{{ old('url') }}" selected>{{ old('url', 'Pilih URL') }}</option>
                            <option value="haribahagia.pw">haribahagia.pw</option>
                            <option value="inv.co.id">inv.co.id</option>
                            <option value="momenspesial.my.id">momenspesial.my.id</option>
                            <option value="undang.pw">undang.pw</option>
                        </select>
                        @error('url')
                        <div id="validationServer03Feedback" class="invalid-feedback">
                            {{ $message }}
                          </div>
                        @enderror
                        </div>
                        <div class="col-2"></div>
                          <div class="mb-3 col-5">
                              <label for="sub_domain" class="form-label">Subdomain</label>
                              <input type="text" class="form-control @error('sub_domain') is-invalid @enderror" id="sub_domain" name="sub_domain" value="{{ old('sub_domain') }}">
                              @error('sub_domain')
                              <div id="validationServer03Feedback" class="invalid-feedback">
                                  {{ $message }}
                              </div>
                              @enderror
                          </div>
                          </div>
                          <div class="d-flex justify-content-center">
                            <div class="mb-3">
                              <label for="formFileSm" class="form-label">Logo</label>
                              <input type="file" class="form-control @error('logo') is-invalid @enderror" id="formFileSm" name="logo" value="{{ old('logo') }}" onchange="imagePreview()">
                              <img src="" class="img-logo img-fluid mt-4 col-sm-2">
                              @error('logo')
                              <div id="validationServer03Feedback" class="invalid-feedback">
                                  {{ $message }}
                                </div>
                              @enderror
                            </div>
                        </div>
                    </div>


                    <div class="mb-3">
                        <input type="hidden" class="form-control" id="status" value="Rekanan Inv" name="status" value="{{ old('status') }}">
                    </div>
                    <div class="mb-3">
                        <input type="hidden" class="form-control" id="author" value="{{ $a }}" name="author" value="{{ old('author') }}">
                    </div>
                    <button type="submit" class="btn btn-primary mb-8 col-6">Submit</button>
                  </form>

                  @if (session()->has('berhasilTambahData'))
                  <div class="alert alert-success alert-dismissible fade show mt-4" role="alert">
                    {{ session('berhasilTambahData') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                  </div>
                  @endif

            </div>
        </div>
    </div>
    <script>
      function imagePreview(){
              const image = document.querySelector('#formFileSm')
              const imgPreview = document.querySelector('.img-logo')

              imgPreview.style.display = 'block';

              const oFReader = new FileReader();
              oFReader.readAsDataURL(image.files[0]);

              oFReader.onload = function(oFREvent){
                  imgPreview.src = oFREvent.target.result;
              }
          }
    </script>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</html>
