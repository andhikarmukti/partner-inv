@extends('layouts.main')

@section('container')

<div class="container">
    <div class="row">
        <div class="col-lg-6">

            <form class="p-2" method="post" action="/edit-footer">
                @csrf
                <div class="row">
                    <div class="col mb-3">
                    <label for="email" class="form-label">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="@if($footer !== null) {{ $footer->email }} @endif">
                    </div>
                    <div class="col mb-3">
                    <label for="whatsapp" class="form-label">WhatsApp</label>
                    <input type="text" class="form-control" id="whatsapp" name="whatsapp" placeholder="@if($footer !== null) {{ $footer->whatsapp }} @endif">
                    </div>
                    <div class="col mb-3">
                    <label for="instagram" class="form-label">Instagram</label>
                    <input type="text" class="form-control" id="instagram" name="instagram" placeholder="@if($footer !== null) {{ $footer->instagram }} @endif">
                    </div>
                </div>
                <div class="">
                    <label for="about_us" class="">About Us</label>
                    <textarea class="form-control" id="about_us" name="about_us" style="height: 100px" placeholder="@if($footer !== null) {{ $footer->about_us }} @endif"></textarea>
                </div> 
                <button type="submit" class="btn btn-primary mt-3">Submit</button>
              </form>

        </div>
    </div>
</div>

@endsection