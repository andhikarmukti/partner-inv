  @extends('layouts.main')

  @section('container')
  <!-- Main content -->
  <section class="content">
      <div class="container-fluid">
        <marquee Onmouseover="this.stop()" onMouseOut="this.start()" bgcolor="sky-blue"><h4>{{ $time }}, {{ auth()->user()->name }}! Discount 30% untuk semua paket produk bagi setiap rekanan inv.co.id</h4></marquee>
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->

    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="bg-white rounded mb-4 p-3">
                    Total orderan bulan ini : {{ $jumlahOrderBulanIni }}
                    <br>
                    Jumlah profit bulan ini : Rp. {{ number_format($profitBulanIni,0,',','.')  }}
                </div>
            </div>
        </div>
    </div>

  <div class="container-lg">
      <div class="row">
          <div class="col">
              <div id="container"></div>
          </div>
      </div>
  </div>


{{-- Highcharts --}}
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script>
    Highcharts.chart('container', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Analytic Order 2021'
        },
        // subtitle: {
        //     text: 'Source: WorldClimate.com'
        // },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Jumlah Orderan'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Jumlah Orderan',
            data: [
            {{ $jumlahOrderJanuari }},
            {{ $jumlahOrderFebruari }},
            {{ $jumlahOrderMaret }},
            {{ $jumlahOrderApril }},
            {{ $jumlahOrderMei }},
            {{ $jumlahOrderJuni }},
            {{ $jumlahOrderJuli }},
            {{ $jumlahOrderAgustus }},
            {{ $jumlahOrderSeptember }},
            {{ $jumlahOrderOktober}},
            {{ $jumlahOrderNovember }},
            {{ $jumlahOrderDesember }}
            ]
        },
        {
            name: 'Total Profit',
            data: [
            {{ $profitJanuari }},
            {{ $profitFebruari }},
            {{ $profitMaret }},
            {{ $profitApril }},
            {{ $profitMei }},
            {{ $profitJuni }},
            {{ $profitJuli }},
            {{ $profitAgustus }},
            {{ $profitSeptember }},
            {{ $profitOktober}},
            {{ $profitNovember }},
            {{ $profitDesember }}
            ]
        }   ]
    });
</script>
  @endsection
