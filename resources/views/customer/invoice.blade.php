@extends('layouts.main')

@section('container')


<div class="container">
    <div class="d-flex justify-content-center row">
        <div class="col-md-8 mt-5">
            <div class="mb-3">
                <a href="{{ Request::is('new-customer*') ? '/new-customer' : '/list-customer' }}" class="btn btn-primary">Kembali</a>
            </div>
            <div class="p-3 bg-white rounded mb-5">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="text-uppercase">Invoice</h1>
                        <div class="billed"><span class="font-weight-bold text-uppercase">Billed:</span><span class="ml-1">{{ $data->rekanan }}</span></div>
                        <div class="billed"><span class="font-weight-bold text-uppercase">Date:</span><span class="ml-1">{{ $data->created_at }}</span></div>
                        <div class="billed"><span class="font-weight-bold text-uppercase">Order ID:</span><span class="ml-1">{{ $data->id }}</span></div>
                        <div class="billed"><span class="font-weight-bold text-uppercase">No. Invoice:</span><span class="ml-1">{{ $no_invoice }}</span></div>
                    </div>
                    <div class="col-md-6 text-right mt-3">
                        <h4 class="text-info mb-0">Wedding Invitation</h4><span>inv.co.id</span>
                        <hr>
                        <div class="d-flex">
                            <p class="mr-4">Status pembayaran : </p>
                            <h3 @if($data->pembayaran == 'belum dibayar')
                                style="color:red;font-style:italic"
                                @elseif($data->pembayaran == 'Expired')
                                style="color:red"
                                @else
                                style="color:green"
                            @endif>{{ $data->pembayaran }}</h3>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="mt-3">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Demo</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($data->rekanan == 'Admin')
                                <tr>
                                    <td>Paket Wedding {{ $data->paket_wedding }}</td>
                                    <td>{{ $data->demo }}</td>
                                    <td>Rp. {{ number_format($data->harga_paket,0,',','.') }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="font-weight: bold">Total</td>
                                    <td style="font-weight: bold">Rp. {{ number_format($data->harga_paket,0,',','.') }}</td>
                                </tr>
                                @else
                                <tr>
                                    <td>Paket Wedding {{ $data->paket_wedding }}</td>
                                    <td>{{ $data->demo }}</td>
                                    <td>Rp. {{ number_format($data->harga_paket,0,',','.') }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Discount 30%</td>
                                    <td>Rp. {{ number_format($discount,0,',','.') }}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td style="font-weight: bold">Total</td>
                                    <td style="font-weight: bold">Rp. {{ number_format($data->harga_paket - $discount,0,',','.') }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if($data->pembayaran == 'belum dibayar')
<div class="container">
    <div class="d-flex justify-content-center row">
        <div class="col-md-8 mt-5">
            <h3>Metode Pembayaran : </h3>
            <div class="p-3 bg-white rounded">
                <form action="{{ Request::is('new-customer*') ? '/new-customer/metode-pembayaran' : '/list-customer/metode-pembayaran' }}" method="post">
                    @csrf
                    <label for="BCA">BCA</label>
                    <input type="radio" value="BCA" class="mr-5" id="BCA" name="metode_pembayaran">
                    <label for="Mandiri">Mandiri</label>
                    <input type="radio" value="Mandiri" class="mr-5" id="Mandiri" name="metode_pembayaran">
                    <label for="BRI">BRI</label>
                    <input type="radio" value="BRI" class="mr-5" id="BRI" name="metode_pembayaran">
                    <label for="BNI">BNI</label>
                    <input type="radio" value="BNI" class="mr-5" id="BNI" name="metode_pembayaran">
                    <label for="OVO">OVO</label>
                    <input type="radio" value="OVO" class="mr-5" id="OVO" name="metode_pembayaran">
                <button class="btn btn-danger btn-sm" type="submit">Bayar Sekarang</button>
                </form>
                @if(session()->has('metodePembayaranNull'))
                <div class="alert alert-danger alert-dismissible fade show mt-4" role="alert">
                    {{ session('metodePembayaranNull') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endif
@endsection
