@extends('layouts.main')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-lg-4">

            <a class="btn btn-primary mt-2" href="{{ url()->previous() }}">Kembali</a>

            <div class="card mt-5 p-3">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">{{ $data['namaBank'] == 'OVO' ? 'No. HP' : 'No. Rekening' }}<h4 style="font-weight: bold">{{ $data['noRek'] }}</h4></li>
                  <li class="list-group-item">{{ 'a/n : ' . $data['atasNama'] }}</li>
                  <li class="list-group-item">{{ $data['namaBank'] }}</li>
                  <input type="hidden" class="list-group-item" value="{{ $cust['pembayaran'] }}" id="status-pembayaran">
                  <input type="hidden" class="list-group-item" value="{{ $cust['id'] }}" name="id" id="idCustomer">
                </ul>
            </div>
            <div class="card mt-2 p-4">
                <h5>Silahkan kirimkan bukti transfer dan order ID ke WhatsApp admin : <a href="https://api.whatsapp.com/send?phone=6281234567889&text=Halo%20admin%20INV,%20saya%20mau%20memberikan%20bukti%20transfer%20untuk%20order%20ID%20{{ $cust->id }}." target="_blank">081234567889</a></h5>
            </div>
            <div class="">
                Batas waktu transfer :
                <h5 id="timer"></h5>
            </div>
        </div>
    </div>
</div>




  <script>
    var countDownDate = new Date("{{ $cust['deadline'] }}").getTime();

    // Memperbarui hitungan mundur setiap 1 detik
    var x = setInterval(function() {

    // Untuk mendapatkan tanggal dan waktu hari ini
    var now = new Date().getTime();

    // Temukan jarak antara sekarang dan tanggal hitung mundur
    var distance = countDownDate - now;

    // Perhitungan waktu untuk hari, jam, menit dan detik
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Keluarkan hasil dalam elemen dengan id = "timer"
    $("#timer").html(`
    <h5 id="timer"> `+ hours +` Jam , `+ minutes +` Menit , `+ seconds +` Detik  </h5>
    `)

    // Jika hitungan mundur selesai, tulis beberapa teks
    var statusPembayaran = $('#status-pembayaran').attr('value');
    if (statusPembayaran == 'lunas') {
        clearInterval(x);
        $("#timer").html(`
        <h5 id="timer" style="color:green" value="lunas"> Lunas </h5>
        `)
    }else if (distance < 0) {
        clearInterval(x);
        $("#timer").html(`
        <h5 id="timer" style="color:red" value="expired"> Expired </h5>
        `)
        var id_customer = $('#idCustomer').attr('value');
        $.ajax({
            type : 'GET',
            dataType : 'JSON',
            url : '/list-customer/metode-pembayaran',
            data : {
                status_pembayaran : 'Expired',
                id_customer : id_customer
            },
            success : function(result){
                console.log('ok');
                $("#timer").html(`
                <h5 id="timer" style="color:red" value="expired"> Expired </h5>
                `)
            }

        })
    }
    }, 1000);
  </script>
@endsection
