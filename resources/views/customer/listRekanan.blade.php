@extends('layouts.main')

@section('container')

{{-- <div class="container"> --}}
    <div class="row">
        <div class="col">
            @if(session()->has('BerhasilHapusRekanan'))
            <div class="alert alert-danger alert-dismissible fade show mt-4" role="alert">
                {{ session('BerhasilHapusRekanan') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif
            <h2 class="mt-5 text-center mb-3">List Rekanan</h2>
            <form action="list-rekanan" method="GET">
                <div class="input-group mb-3 mt-4">
                  <div class="col-6">
                    <input type="text" class="form-control" placeholder="Search.." autofocus id="keyword" name="keyword">
                  </div>
                  <button class="btn btn-outline-primary" type="submit" id="tombol_search">Search</button>
                </div>
              </form>
            <table class="table table-hover table-responsive-lg">
                <thead>
                  <tr>
                    <th scope="col">Tanggal Join</th>
                    <th scope="col">Nama Rekanan</th>
                    <th scope="col">Domisili</th>
                    <th scope="col">No. WA</th>
                    <th scope="col">Instagram</th>
                    <th scope="col">Subdomain</th>
                    <th scope="col">URL</th>
                    <th scope="col">Email</th>
                    <th scope="col">Logo</th>
                    {{-- <th scope="col">Action</th> --}}
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $d)
                    <tr>
                        <td>{{ $d->created_at }}</td>
                        <td>{{ $d->name }}</td>
                        <td>{{ $d->domisili }}</td>
                        <td>{{ $d->whatsapp }}</td>
                        <td>{{ $d->instagram }}</td>
                        <td>{{ $d->sub_domain }}</td>
                        <td>{{ $d->url_landing_page }}</td>
                        <td>{{ $d->email }}</td>
                        <td>
                            <a href="{{ asset('storage/' . $d->logo) }}" target="_blank">cek logo</a>
                        </td>
                        {{-- <td>
                            <form action="list-rekanan/{{ $d->id }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="badge badge-danger border-0" onclick="return confirm('anda yakin?')">delete</button>
                            </form>
                        </td> --}}
                    </tr>
                    @endforeach
                </tbody>
              </table>
        </div>
    </div>
{{-- </div> --}}

@endsection
