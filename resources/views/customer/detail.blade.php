@extends('layouts.main')

@section('container')
<div class="container">
    <div class="row">
        <div class="col">

            @if(session()->has('berhasilHapus'))
            <div class="alert alert-success alert-dismissible fade show mt-4" role="alert">
                {{ session('berhasilHapus') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif

            <table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Order ID</th>
                    <th scope="col">Paket Wedding</th>
                    <th scope="col">Demo</th>
                    <th scope="col">URL Domain</th>
                    <th scope="col">Subdomain</th>
                    <th scope="col">Pembayaran</th>
                    <th scope="col">Pengerjaan</th>
                    <th scope="col">Rekanan</th>
                    <th scope="col">action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $d)
                    <tr>
                      <th scope="row">{{ $d->id }}</th>
                      <td>{{ $d->paket_wedding }}</td>
                      <td>{{ $d->demo }}</td>
                      <td>{{ $d->url_domain }}</td>
                      <td>{{ $d->sub_domain }}</td>
                      <td @if($d->pembayaran == 'belum dibayar') style="color:red;font-weight:bold" @elseif($d->pembayaran == 'lunas') style="color:green;font-weight:bold" @endif>{{ $d->pembayaran }}</td>
                      <td @if($d->pengerjaan == 'pending') style="color:black;font-weight:bold" @elseif($d->pengerjaan == 'in progress') style="color:orange;font-weight:bold" @else style="color:green;font-weight:bold" @endif>{{ $d->pengerjaan }}</td>
                      <td>{{ $d->rekanan }}</td>
                      <td>
                        <div class="d-flex">
                          <form action="detail/" method="post" class="mr-1">
                            @csrf
                            <button type="submit" class="badge badge-primary border-0">Detail</button>
                          </form>
                          <form action="edit/{{ $d->id }}" method="post" class="mr-1">
                            @csrf
                            <button type="submit" class="badge badge-warning border-0">Edit</button>
                          </form>
                          <form action="delete/{{ $d->id }}" method="post">
                            @csrf
                            <button type="submit" class="badge badge-danger border-0">Delete</button>
                          </form>
                        </div>
                      </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection