@extends('layouts.main')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col">
                <a href="{{ URL::previous() }}" class="badge bg-primary">kembali</a>
                <h4 class="text-center mb-5">Gallery Photos</h4>

                @if(session()->has('berhasil'))
                <div class="alert alert-success alert-dismissible fade show mt-4" role="alert">
                    {{ session('berhasil') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                @endif

                <div class="card mt-5">
                    <div class="card-body">
                        <form action="/list-customer/gallery" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <input type="hidden" value="" name="customer_invoice">
                            <input type="hidden" value="{{ $gallery['photo_1'] }}" name="photo_1_lama">
                            <input type="hidden" value="{{ $gallery['photo_2'] }}" name="photo_2_lama">
                            <input type="hidden" value="{{ $gallery['photo_3'] }}" name="photo_3_lama">
                            <input type="hidden" value="{{ $gallery['photo_4'] }}" name="photo_4_lama">
                            <input type="hidden" value="{{ $gallery['photo_5'] }}" name="photo_5_lama">
                            <input type="hidden" value="{{ $gallery['photo_6'] }}" name="photo_6_lama">
                            <input type="hidden" value="{{ $gallery['photo_7'] }}" name="photo_7_lama">
                            <input type="hidden" value="{{ $gallery['photo_8'] }}" name="photo_8_lama">
                            <input type="hidden" value="{{ $gallery['photo_9'] }}" name="photo_9_lama">
                            <input type="hidden" value="{{ $gallery['photo_10'] }}" name="photo_10_lama">
                            <input type="hidden" value="{{ $id }}" name="id">
                            <div class="card p-2 mb-3">
                                <div class="mb-3">
                                    <small for="gambar_wanita" class="form-label">Photo 1</small>
                                    <input class="form-control form-control-sm @error('photo_1') is-invalid @enderror" id="photo_1" type="file" name="photo_1" value="{{ $gallery['photo_1'] }}">
                                    <img src="{{ asset('storage/' . $gallery['photo_1']) }}" alt="" class="img-fluid mt-4 col-sm-6">
                                    @error('photo_1')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="card p-2 mb-3">
                                <div class="mb-3">
                                    <small for="gambar_wanita" class="form-label">Photo 2</small>
                                    <input class="form-control form-control-sm @error('gambar_wanita') is-invalid @enderror" id="photo_2" type="file" name="photo_2">
                                    <img src="{{ asset('storage/' . $gallery['photo_2']) }}" alt="" class="img-fluid mt-4 col-sm-6">
                                    @error('gambar_wanita')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="card p-2 mb-3">
                                <div class="mb-3">
                                    <small for="gambar_wanita" class="form-label">Photo 3</small>
                                    <input class="form-control form-control-sm @error('gambar_wanita') is-invalid @enderror" id="photo_3" type="file" name="photo_3">
                                    <img src="{{ asset('storage/' . $gallery['photo_3']) }}" alt="" class="img-fluid mt-4 col-sm-6">
                                    @error('gambar_wanita')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="card p-2 mb-3">
                                <div class="mb-3">
                                    <small for="gambar_wanita" class="form-label">Photo 4</small>
                                    <input class="form-control form-control-sm @error('gambar_wanita') is-invalid @enderror" id="photo_4" type="file" name="photo_4">
                                    <img src="{{ asset('storage/' . $gallery['photo_4']) }}" alt="" class="img-fluid mt-4 col-sm-6">
                                    @error('gambar_wanita')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="card p-2 mb-3">
                                <div class="mb-3">
                                    <small for="gambar_wanita" class="form-label">Photo 5</small>
                                    <input class="form-control form-control-sm @error('gambar_wanita') is-invalid @enderror" id="photo_5" type="file" name="photo_5">
                                    <img src="{{ asset('storage/' . $gallery['photo_5']) }}" alt="" class="img-fluid mt-4 col-sm-6">
                                    @error('gambar_wanita')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="card p-2 mb-3">
                                <div class="mb-3">
                                    <small for="gambar_wanita" class="form-label">Photo 6</small>
                                    <input class="form-control form-control-sm @error('gambar_wanita') is-invalid @enderror" id="photo_6" type="file" name="photo_6">
                                    <img src="{{ asset('storage/' . $gallery['photo_6']) }}" alt="" class="img-fluid mt-4 col-sm-6">
                                    @error('gambar_wanita')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="card p-2 mb-3">
                                <div class="mb-3">
                                    <small for="gambar_wanita" class="form-label">Photo 7</small>
                                    <input class="form-control form-control-sm @error('gambar_wanita') is-invalid @enderror" id="photo_7" type="file" name="photo_7">
                                    <img src="{{ asset('storage/' . $gallery['photo_7']) }}" alt="" class="img-fluid mt-4 col-sm-6">
                                    @error('gambar_wanita')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="card p-2 mb-3">
                                <div class="mb-3">
                                    <small for="gambar_wanita" class="form-label">Photo 8</small>
                                    <input class="form-control form-control-sm @error('gambar_wanita') is-invalid @enderror" id="photo_8" type="file" name="photo_8">
                                    <img src="{{ asset('storage/' . $gallery['photo_8']) }}" alt="" class="img-fluid mt-4 col-sm-6">
                                    @error('gambar_wanita')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="card p-2 mb-3">
                                <div class="mb-3">
                                    <small for="gambar_wanita" class="form-label">Photo 9</small>
                                    <input class="form-control form-control-sm @error('gambar_wanita') is-invalid @enderror" id="photo_9" type="file" name="photo_9">
                                    <img src="{{ asset('storage/' . $gallery['photo_9']) }}" alt="" class="img-fluid mt-4 col-sm-6">
                                    @error('gambar_wanita')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="card p-2 mb-3">
                                <div class="mb-3">
                                    <small for="gambar_wanita" class="form-label">Photo 10</small>
                                    <input class="form-control form-control-sm @error('gambar_wanita') is-invalid @enderror" id="photo_10" type="file" name="photo_10">
                                    <img src="{{ asset('storage/' . $gallery['photo_10']) }}" alt="" class="img-fluid mt-4 col-sm-6">
                                    @error('gambar_wanita')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function imagePreview(){
            const image = document.querySelector('#gambar_pria')
            const imgPreview = document.querySelector('.photo_preview')

            imgPreview.style.display = 'block';

            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);

            oFReader.onload = function(oFREvent){
                imgPreview.src = oFREvent.target.result;
            }
        }
    </script>
@endsection