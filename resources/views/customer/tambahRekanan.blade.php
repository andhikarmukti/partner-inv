@extends('layouts.main')

@section('container')
<div class="container">
    <div class="row">
        <div class="col-6">
            <form enctype="multipart/form-data" action="/tambah-rekanan" method="post" class="mt-4">
                @csrf
                <div class="mb-3">
                    <label for="tanggal_join" class="form-label">Tanggal Join</label>
                    <input type="date" class="form-control @error('tanggal_join') is-invalid @enderror" id="tanggalJoin"
                        name="tanggal_join" value="{{ old('tanggal_join') }}">
                    @error('tanggal_join')
                    <div id="validationServer03Feedback" class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="nama_rekanan" class="form-label">Nama Rekanan</label>
                    <input type="text" class="form-control @error('nama_rekanan') is-invalid @enderror" id="namaRekanan"
                        name="nama_rekanan" value="{{ old('nama_rekanan') }}">
                    @error('nama_rekanan')
                    <div id="validationServer03Feedback" class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="d-flex">
                    <div class="mb-3 col-5">
                        <label for="nama_usaha" class="form-label">Nama Usaha</label>
                        <input type="text" class="form-control @error('nama_usaha') is-invalid @enderror" id="namaUsaha"
                            name="nama_usaha" value="{{ old('nama_usaha') }}">
                        @error('nama_usaha')
                        <div id="validationServer03Feedback" class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="col-2"></div>
                    <div class="mb-3 col-5">
                        <label for="jenis_usaha" class="form-label">Jenis Usaha</label>
                        <input type="text" class="form-control @error('jenis_usaha') is-invalid @enderror"
                            id="jenisUsaha" name="jenis_usaha" value="{{ old('jenis_usaha') }}">
                        @error('jenis_usaha')
                        <div id="validationServer03Feedback" class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <hr>
                <div class="mb-3 d-flex">
                    <div class="col-4">
                        <select name="socialMedia1" id="socialMedia1" class="mb-2 badge badge-info">
                            <option value="whatsapp" selected>WhatsApp</option>
                        </select>
                        <input type="text" class="form-control @error('sosmed1') is-invalid @enderror" id="sosmed1"
                            name="sosmed1" value="{{ old('sosmed1') }}">
                        @error('sosmed1')
                        <div id="validationServer03Feedback" class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="col-4">
                        <select name="socialMedia2" id="socialMedia2" class="mb-2 badge badge-info">
                            <option value="whatsapp">WhatsApp</option>
                            <option value="instagram" selected>Instagram</option>
                            <option value="facebook">Facebook</option>
                            <option value="twitter">Twitter</option>
                        </select>
                        <input type="text" class="form-control @error('sosmed2') is-invalid @enderror" id="whatsapp"
                            name="sosmed2" value="{{ old('sosmed2') }}">
                    </div>
                    <div class="col-4">
                        <select name="socialMedia3" id="socialMedia3" class="mb-2 badge badge-info">
                            <option value="whatsapp">WhatsApp</option>
                            <option value="instagram">Instagram</option>
                            <option value="facebook" selected>Facebook</option>
                            <option value="twitter">Twitter</option>
                        </select>
                        <input type="text" class="form-control @error('sosmed3') is-invalid @enderror" id="whatsapp"
                            name="sosmed3" value="{{ old('sosmed3') }}">
                    </div>
                    @error('jenisUsaha')
                    <div id="validationServer03Feedback" class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="domisili" class="form-label">Domisili</label>
                    <input type="text" class="form-control @error('domisili') is-invalid @enderror" id="domisili"
                        name="domisili" value="{{ old('domisili') }}">
                    @error('domisili')
                    <div id="validationServer03Feedback" class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <hr>
                <div class="d-flex">
                    <div class="mb-3 col-5">
                        <label for="url_landing_page" class="form-label">URL Landing page</label>
                        <input type="text" class="form-control @error('url_landing_page') is-invalid @enderror"
                            id="url_landing_page" name="url_landing_page" value="{{ old('url_landing_page') }}">
                        @error('url_landing_page')
                        <div id="validationServer03Feedback" class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="col-2"></div>
                    <div class="mb-3 col-5">
                        <label for="sub_domain" class="form-label">Subdomain</label>
                        <input type="text" class="form-control @error('sub_domain') is-invalid @enderror"
                            id="sub_domain" name="sub_domain" value="{{ old('sub_domain') }}">
                        @error('sub_domain')
                        <div id="validationServer03Feedback" class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>

                <div class="d-flex">
                    <div class="mb-3 col-6">
                        <label for="formFileSm" class="form-label">Logo</label>
                        <input type="file" class="form-control @error('logo') is-invalid @enderror" id="formFileSm"
                            name="logo" value="{{ old('logo') }}">
                        @error('logo')
                        <div id="validationServer03Feedback" class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="col-1"></div>
                    <div class="mb-3 cl-5">
                        <label for="status" class="form-label">Status</label>
                        <input type="text" class="form-control @error('status') is-invalid @enderror" id="status"
                            name="status" value="{{ old('status') }}">
                        @error('status')
                        <div id="validationServer03Feedback" class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>

                <div class="mb-3">
                    <input type="hidden" class="form-control" id="status" value="{{ $a }}" name="author"
                        value="{{ old('author') }}">
                </div>
                <button type="submit" class="btn btn-primary mb-8">Submit</button>
            </form>

            @if (session()->has('berhasilTambahData'))
            <div class="alert alert-success alert-dismissible fade show mt-4" role="alert">
                {{ session('berhasilTambahData') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif

        </div>
    </div>
</div>
@endsection
