@extends('layouts.main')

@section('container')
    <div class="row">
        <div class="col">
        <h2 class="text-center m-5">List Customer</h2>
        <div class="col-lg-6">
            @if(session()->has('berhasilHapus'))
            <div class="alert alert-danger alert-dismissible fade show mt-4" role="alert">
                {{ session('berhasilHapus') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif

            @if(session()->has('berhasilEdit'))
            <div class="alert alert-warning alert-dismissible fade show mt-4" role="alert">
                {{ session('berhasilEdit') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif

            @if(session()->has('BerhasilLunas'))
            <div class="alert alert-success alert-dismissible fade show mt-4" role="alert">
                {{ session('BerhasilLunas') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif

            @if(session()->has('BerhasilInprogress'))
            <div class="alert alert-secondary alert-dismissible fade show mt-4" role="alert">
                {{ session('BerhasilInprogress') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif

            @if(session()->has('BerhasilCompleted'))
            <div class="alert alert-success alert-dismissible fade show mt-4" role="alert">
                {{ session('BerhasilCompleted') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif

            @if(session()->has('BerhasilRevision'))
            <div class="alert alert-danger alert-dismissible fade show mt-4" role="alert">
                {{ session('BerhasilRevision') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif
          </div>

            <form action="list-customer" method="GET">
              <div class="input-group mb-3 mt-4">
                <div class="col-6">
                  <input type="text" class="form-control" placeholder="Search.." autofocus id="keyword" name="keyword">
                </div>
                <button class="btn btn-outline-primary" type="submit" id="tombol_search">Search</button>
              </div>
            </form>

            <table class="table table-hover table-responsive-lg">
              <thead>
                  <tr>
                    <th scope="col">Order ID</th>
                    <th scope="col">Paket Wedding</th>
                    <th scope="col">Demo</th>
                    <th scope="col">URL Domain</th>
                    <th scope="col">Subdomain</th>
                    <th scope="col">Pembayaran</th>
                    <th scope="col">Pengerjaan</th>
                    @if(auth()->user()->role == 'admin')
                    <th scope="col">Rekanan</th>
                    @endif
                    <th scope="col">action</th>
                  </tr>
                </thead>
                <tbody id="tbody">
                  @foreach ($data as $d)
                    <tr>
                      <th scope="row">{{ $d->id }}</th>
                      <td>{{ $d->paket_wedding }}</td>
                      <td>{{ $d->demo }}</td>
                      <td>{{ $d->url_domain }}</td>
                      <td>{{ $d->sub_domain }}</td>
                      <td
                        @if($d->pembayaran == 'belum dibayar') style="color:brown;font-weight:bold"
                        @elseif($d->pembayaran == 'lunas') style="color:green;font-weight:bold"
                        @elseif($d->pembayaran == 'Expired') style="color:red;font-weight:bold"
                        @endif>{{ $d->pembayaran }}
                      </td>
                      <td @if($d->pengerjaan == 'pending') style="color:orange;font-weight:bold" @elseif($d->pengerjaan == 'in progress') style="color:orange;font-weight:bold" @else style="color:green;font-weight:bold" @endif>{{ $d->pengerjaan }}</td>
                      @if(auth()->user()->role == 'admin')
                      <td>{{ $d->rekanan }}</td>
                      @endif
                      <td>

                        <div class="mb-1">
                          <button href="{{ $d->id }}" class="badge badge-primary mr-1 border-0 tombolDetail" data-bs-toggle="modal" data-bs-target="#exampleModal">Detail</button>
                          <a href="/list-customer/invoice/{{ $d->id }}" class="badge badge-secondary mr-1 border-0">Invoice</a>
                          <a href="/list-customer/edit/{{ $d->id }}" class="badge badge-warning border-0 mr-1">Edit</a>
                          <a href="/list-customer/gallery/{{ $d->id }}" class="badge badge-warning border-0 mr-1">Gallery</a>
                        </div>
                        @if(auth()->user()->role == 'admin')
                        <div class="d-flex mb-1">
                            <a href="delete/{{ $d->id }}" class="badge badge-danger border-0 mr-1" onclick="return confirm('are you sure?')">Delete</a>
                            <a href="lunas/{{ $d->id }}" class="badge badge-info border-0 mr-1" onclick="return confirm('are you sure?')">Lunas</a>
                        </div>
                        <div class="d-flex">
                            <a href="inprogress/{{ $d->id }}" class="badge badge-dark border-0 mr-1" onclick="return confirm('are you sure?')">In Progress</a>
                            <a href="completed/{{ $d->id }}" class="badge badge-success border-0 mr-1" onclick="return confirm('are you sure?')">Completed</a>
                            <a href="revision/{{ $d->id }}" class="badge badge-danger border-0 mr-1" onclick="return confirm('are you sure?')">Revision</a>
                        </div>
                        @endif
                      </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
          <div class="d-flex">

            <div class="card-lg">
              <div class="card-body-lg">
                <div class="card p-3">
                  <p id="tanggalAkad">Tanggal Akad :</p>
                  <p id="jamAkad">Jam Akad :</p>
                  <p id="tanggalResepsi">Tanggal Resepsi :</p>
                  <p id="jamResepsi">Jam Resepsi :</p>
                  <p id="alamatResepsi">Alamat Resepsi :</p>
                </div>

                <img src="1" class="card-img-top gambarPria" alt="gambarPria">
                <div class="card-body">

                  <ul>

                    <li>
                      <p id="namaLengkapPria">Nama lengkap : </p>
                    </li>

                    <li>
                      <p id="namaPanggilanPria">Nama panggilan : </p>
                    </li>

                    <li>
                      <p id="namaIbuPria">Nama Ibu : </p>
                    </li>

                    <li>
                      <p id="namaAyahPria">Nama Ayah : </p>
                    </li>

                    <li>
                      <p id="putreKePria">Putra ke : </p>
                    </li>

                    <li>
                      <p id="instagramPria">Instagram : </p>
                    </li>

                  </ul>
                </div>
              </div>

              <div class="card-body">
                <img src="1" class="card-img-top gambarWanita" alt="gambarWanita">
                <div class="card-body">
                  <ul>

                    <li>
                      <p id="namaLengkapWanita">Nama lengkap : </p>
                    </li>

                    <li>
                      <p id="namaPanggilanWanita">Nama panggilan : </p>
                    </li>

                    <li>
                      <p id="namaIbuWanita">Nama Ibu : </p>
                    </li>

                    <li>
                      <p id="namaAyahWanita">Nama Ayah : </p>
                    </li>

                    <li>
                      <p id="putreKeWanita">Putra ke : </p>
                    </li>

                    <li>
                      <p id="instagramWanita">Instagram : </p>
                    </li>

                  </ul>
                </div>
              </div>
            </div>

          </div> {{-- d-flex close div --}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
    $('.tombolDetail').click(function(){
        console.log('ok');
        var detail = $(this).attr('href');
        console.log(detail);
        $.ajax({
            type : 'GET',
            dataType : 'JSON',
            url : 'detailcust',
            data : {
                detail : detail
            },
            success : function(result){
                console.log(result);

                //data pria
                $('#namaLengkapPria').html(`
                    <p>Nama lengkap : `+ result[0] +`</p>
                `);

                $('#namaPanggilanPria').html(`
                    <p>Nama Panggilan : `+ result[1] +`</p>
                `);

                $('#namaIbuPria').html(`
                    <p>Nama Ibu : `+ result[2] +`</p>
                `);

                $('#namaAyahPria').html(`
                    <p>Nama Ayah : `+ result[3] +`</p>
                `);

                $('#putreKePria').html(`
                    <p>Putra Ke : `+ result[4] +`</p>
                `);

                $('#instagramPria').html(`
                    <p>Instagram : `+ result[5] +`</p>
                `);

                $('.gambarPria').attr('src', 'storage/' + result[6]);

                //data wanita
                $('#namaLengkapWanita').html(`
                    <p>Nama lengkap : `+ result[7] +`</p>
                `);

                $('#namaPanggilanWanita').html(`
                    <p>Nama Panggilan : `+ result[8] +`</p>
                `);

                $('#namaIbuWanita').html(`
                    <p>Nama Ibu : `+ result[9] +`</p>
                `);

                $('#namaAyahWanita').html(`
                    <p>Nama Ayah : `+ result[10] +`</p>
                `);

                $('#putreKeWanita').html(`
                    <p>Putri Ke : `+ result[11] +`</p>
                `);

                $('#instagramWanita').html(`
                    <p>Instagram : `+ result[12] +`</p>
                `);

                $('#tanggalAkad').html(`
                    <p>Tanggal Akad : `+ result[13] +`</p>
                `);

                $('#jamAkad').html(`
                    <p>Jam Akad : `+ result[14] +`</p>
                `);

                $('#tanggalResepsi').html(`
                    <p>Tanggal Resepsi : `+ result[15] +`</p>
                `);

                $('#jamResepsi').html(`
                    <p>Jam Resepsi : `+ result[16] +`</p>
                `);

                $('#alamatResepsi').html(`
                    <p>Alamat Resepsi : `+ result[17] +`</p>
                `);

                $('.gambarWanita').attr('src', 'storage/' + result[18]);

            }
        })
    })
</script>
@endsection
