<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.6.0.js"
        integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <title>Form Customer {{ $r }}</title>
</head>

<body>

    <form action="/order" method="post" enctype="multipart/form-data">
        @csrf
        <div class="container">
            <div class="row mt-2">
                <div class="col-lg-6">
                    @if(session()->has('berhasilTambahData'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('berhasilTambahData') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    <a href="#" class="btn btn-primary" onclick="window.location.reload()">Nikah lagi?</a>
                    @endif
                    @if(!session()->has('berhasilTambahData'))
                    <div class="card mt-5">
                        <div class="card-body">
                            <h4 class="text-center mb-5">Paket Wedding</h4>
                            <div class="mb-3">
                                <small for="exampleInputEmail1" class="form-label">Paket Wedding</small>
                                <select class="form-select @error('paket_wedding') is-invalid @enderror"
                                    aria-label="Default select example" id="paket_wedding" name="paket_wedding">
                                    <option selected disabled>Silahkan Pilih Paket</option>
                                    <option value="Basic" @if (old('paket_wedding')=='Basic' ) selected @endif>Basic
                                    </option>
                                    <option value="Premium" @if (old('paket_wedding')=='Premium' ) selected @endif>
                                        Premium</option>
                                    <option value="VIP" @if (old('paket_wedding')=='VIP' ) selected @endif>VIP</option>
                                </select>
                                @error('paket_wedding')
                                <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                                {{-- <div id="emailHelp" class="form-text">We'll never share your email with anyone
                                    else.</div> --}}
                            </div>
                            <div class="card p-2">
                                <div class="mb-3">
                                    <small for="demo" class="form-label" id="demoLabel">{{ old('paket_wedding', 'Demo')
                                        }}</small>
                                    <select class="form-select @error('demo') is-invalid @enderror" id="demo"
                                        name="demo">
                                        @if(old('demo'))
                                        <option id="demoOption" @if (old('demo')) value="{{ old('demo') }}" @endif>{{
                                            old('demo') }}</option>
                                        @else
                                        <option selected disabled>Anda belum memilih Paket Wedding</option>
                                        @endif
                                    </select>
                                    @error('demo')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="mb-3">
                                    <small for="url_domain" class="form-label">URL Domain</small>
                                    <select class="form-select @error('url_domain') is-invalid @enderror"
                                        aria-label="Default select example" id="url_domain" name="url_domain">
                                        <option selected disabled>Pilih URL</option>
                                        <option value="haribahagia.pw" @if(old('url_domain')=='haribahagia.pw' )
                                            selected @endif>haribahagia.pw</option>
                                        <option value="inv.co.id" @if(old('url_domain')=='inv.co.id' ) selected @endif>
                                            inv.co.id</option>
                                        <option value="momenspesial.my.id" @if(old('url_domain')=='momenspesial.my.id' )
                                            selected @endif>momenspesial.my.id</option>
                                        <option value="undang.pw" @if(old('url_domain')=='undang.pw' ) selected @endif>
                                            undang.pw</option>
                                    </select>
                                    @error('url_domain')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="mb-3">
                                    <small for="sub_domain" class="form-label">Nama Sub Domain</small>
                                    <input type="text" class="form-control @error('sub_domain') is-invalid @enderror"
                                        placeholder="contoh : (bambang-heni.momenspesial.my.id) atau (udin-prisilia.undang.pw)"
                                        id="sub_domain" name="sub_domain" value="{{ old('sub_domain') }}">
                                    @error('sub_domain')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="mb-3">
                                    <small for="music" class="form-label">Request Music</small>
                                    <input type="text" id="music" name="music"
                                        class="form-control @error('music') is-invalid @enderror"
                                        placeholder="Sertakan link jika ada" value="{{ old('music') }}">
                                    @error('music')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="card mt-5">
                        <div class="card-body">
                            <h4 class="text-center mb-5">Data Mempelai Pria</h4>

                            <div class="card p-2 mb-3">
                                <div class="mb-3">
                                    <small for="nama_lengkap_pria" class="form-label">Nama Lengkap Mempelai Pria</small>
                                    <input type="text"
                                        class="form-control @error('nama_lengkap_pria') is-invalid @enderror"
                                        name="nama_lengkap_pria" id="nama_lengkap_pria"
                                        value="{{ old('nama_lengkap_pria') }}">
                                    @error('nama_lengkap_pria')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <small for="nama_panggilan_pria" class="form-label">Nama Panggilan Pria</small>
                                    <input type="text"
                                        class="form-control @error('nama_panggilan_pria') is-invalid @enderror"
                                        name="nama_panggilan_pria" id="nama_panggilan_pria"
                                        value="{{ old('nama_panggilan_pria') }}">
                                    @error('nama_panggilan_pria')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="col mb-3">
                                    <small for="putra_ke" class="form-label">Putra Ke-</small>
                                    <input type="text" class="form-control @error('putra_ke') is-invalid @enderror"
                                        name="putra_ke" id="putra_ke" placeholder="isi dengan angka saja"
                                        value="{{ old('putra_ke') }}">
                                    @error('putra_ke')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col mb-3">
                                    <small for="instagram_pria" class="form-label">Akun Instagram</small>
                                    <input type="text"
                                        class="form-control @error('instagram_pria') is-invalid @enderror"
                                        name="instagram_pria" id="instagram_pria" value="{{ old('instagram_pria') }}">
                                    @error('instagram_pria')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="d-flex justify-content-center">
                                <div class="mb-3">
                                    <small for="gambar_pria" class="form-label d-flex justify-content-center mt-4">Foto
                                        Mempelai Pria</small>
                                    <input
                                        class="form-control form-control-sm @error('gambar_pria') is-invalid @enderror"
                                        id="gambar_pria" type="file" name="gambar_pria" onchange="imagePreview()">
                                    <img src="" class="gambar_pria_preview img-fluid mt-4 col-sm-12">
                                    @error('gambar_pria')
                                    <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="card mt-5">
                            <div class="card-body">
                                <h4 class="text-center mb-5">Data Mempelai Wanita</h4>

                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="nama_lengkap_wanita" class="form-label">Nama Lengkap Mempelai
                                            Wanita</small>
                                        <input type="text"
                                            class="form-control @error('nama_lengkap_wanita') is-invalid @enderror"
                                            name="nama_lengkap_wanita" id="nama_lengkap_wanita"
                                            value="{{ old('nama_lengkap_wanita') }}">
                                        @error('nama_lengkap_wanita')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <small for="nama_panggilan_wanita" class="form-label">Nama Panggilan
                                            Wanita</small>
                                        <input type="text"
                                            class="form-control @error('nama_panggilan_wanita') is-invalid @enderror"
                                            name="nama_panggilan_wanita" id="nama_panggilan_wanita"
                                            value="{{ old('nama_panggilan_wanita') }}">
                                        @error('nama_panggilan_wanita')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="mb-3 col">
                                        <small for="putri_ke" class="form-label">Putri Ke-</small>
                                        <input type="text" class="form-control @error('putri_ke') is-invalid @enderror"
                                            name="putri_ke" id="putri_ke" placeholder="isi dengan angka saja"
                                            value="{{ old('putri_ke') }}">
                                        @error('putri_ke')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="mb-3 col">
                                        <small for="instagram_wanita" class="form-label">Akun Instagram</small>
                                        <input type="text"
                                            class="form-control @error('instagram_wanita') is-invalid @enderror"
                                            name="instagram_wanita" id="instagram_wanita"
                                            value="{{ old('instagram_wanita') }}">
                                        @error('instagram_wanita')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="d-flex justify-content-center">
                                    <div class="mb-3">
                                        <small for="gambar_wanita"
                                            class="form-label d-flex justify-content-center mt-4">Foto Mempelai
                                            Wanita</small>
                                        <input
                                            class="form-control form-control-sm @error('gambar_wanita') is-invalid @enderror"
                                            id="gambar_wanita" type="file" name="gambar_wanita"
                                            onchange="imagePreviewWanita()">
                                        <img src="" class="gambar_wanita_preview img-fluid mt-4 col-sm-12">
                                        @error('gambar_wanita')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="card mt-5">
                            <div class="card-body">
                                <h4 class="text-center mb-5">Data Orang Tua Mempelai Pria</h4>

                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="ayah_pria" class="form-label">Nama Ayah</small>
                                        <input type="text" class="form-control @error('ayah_pria') is-invalid @enderror"
                                            name="ayah_pria" id="ayah_pria" value="{{ old('ayah_pria') }}">
                                        @error('ayah_pria')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <small for="ibu_pria" class="form-label">Nama Ibu</small>
                                        <input type="text" class="form-control @error('ibu_pria') is-invalid @enderror"
                                            name="ibu_pria" id="ibu_pria" value="{{ old('ibu_pria') }}">
                                        @error('ibu_pria')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="card mt-5">
                            <div class="card-body">
                                <h4 class="text-center mb-5">Data Orang Tua Mempelai Wanita</h4>

                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="ayah_wanita" class="form-label">Nama Ayah</small>
                                        <input type="text"
                                            class="form-control @error('ayah_wanita') is-invalid @enderror"
                                            name="ayah_wanita" id="ayah_wanita" value="{{ old('ayah_wanita') }}">
                                        @error('ayah_wanita')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <small for="ibu_wanita" class="form-label">Nama Ibu</small>
                                        <input type="text"
                                            class="form-control @error('ibu_wanita') is-invalid @enderror"
                                            name="ibu_wanita" id="ibu_wanita" value="{{ old('ibu_wanita') }}">
                                        @error('ibu_wanita')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="card mt-5">
                            <div class="card-body">
                                <h4 class="text-center mb-5">Form Detail Acara</h4>

                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="tanggal_akad" class="form-label">Tanggal Akad / Pemberkatan</small>
                                        <input type="date"
                                            class="form-control @error('tanggal_akad') is-invalid @enderror"
                                            name="tanggal_akad" id="tanggal_akad" value="{{ old('tanggal_akad') }}">
                                        @error('tanggal_akad')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <small for="jam_akad" class="form-label">Jam Akad / Pemberkatan</small>
                                        <input type="time" class="form-control @error('jam_akad') is-invalid @enderror"
                                            name="jam_akad" id="jam_akad" value="{{ old('jam_akad') }}">
                                        @error('jam_akad')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>


                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="tanggal_resepsi" class="form-label">Tanggal Resepsi</small>
                                        <input type="date"
                                            class="form-control @error('tanggal_resepsi') is-invalid @enderror"
                                            name="tanggal_resepsi" id="tanggal_resepsi"
                                            value="{{ old('tanggal_resepsi') }}">
                                        @error('tanggal_resepsi')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <small for="jam_resepsi" class="form-label">Jam Resepsi</small>
                                        <input type="time"
                                            class="form-control @error('jam_resepsi') is-invalid @enderror"
                                            name="jam_resepsi" id="jam_resepsi" value="{{ old('jam_resepsi') }}">
                                        @error('jam_resepsi')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="alamat_akad" class="form-label">Alamat Akad</small>
                                        <textarea class="form-control @error('alamat_akad') is-invalid @enderror"
                                            name="alamat_akad" id="alamat_akad">{{ old('alamat_akad') }}</textarea>
                                        @error('alamat_akad')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <small for="alamat_resepsi" class="form-label">Alamat Resepsi</small>
                                        <textarea class="form-control @error('alamat_resepsi') is-invalid @enderror"
                                            name="alamat_resepsi"
                                            id="alamat_resepsi">{{ old('alamat_resepsi') }}</textarea>
                                        @error('alamat_resepsi')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="quotes" class="form-label d-flex justify-content-center">Ayat Suci &
                                            Quotes</small>
                                        <textarea class="form-control @error('quotes') is-invalid @enderror"
                                            name="quotes" id="quotes">{{ old('quotes') }}</textarea>
                                        @error('quotes')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="catatan_lain"
                                            class="form-label d-flex justify-content-center">Catatan Lain Yang Tidak
                                            Disebutkan Diatas</small>
                                        <textarea class="form-control @error('catatan_lain') is-invalid @enderror"
                                            name="catatan_lain" id="catatan_lain">{{ old('catatan_lain') }}</textarea>
                                        @error('catatan_lain')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                            <input type="hidden" value="{{ $r }}" id="rekanan" name="rekanan">
                        </div>
                        <div class="card mt-5">
                            <div class="card-body">
                                <h4 class="text-center mb-5">Gallery Photo</h4>

                                <input type="hidden" value="" name="customer_invoice">
                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="photo_1" class="form-label">Photo 1</small>
                                        <input
                                            class="form-control form-control-sm @error('photo_1') is-invalid @enderror"
                                            id="photo_1" type="file" name="photo_1">
                                        @error('photo_1')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="photo_2" class="form-label">Photo 2</small>
                                        <input
                                            class="form-control form-control-sm @error('photo_2') is-invalid @enderror"
                                            id="photo_2" type="file" name="photo_2">
                                        @error('photo_2')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="photo_3" class="form-label">Photo 3</small>
                                        <input
                                            class="form-control form-control-sm @error('photo_3') is-invalid @enderror"
                                            id="photo_3" type="file" name="photo_3">
                                        @error('photo_3')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="photo_4" class="form-label">Photo 4</small>
                                        <input
                                            class="form-control form-control-sm @error('photo_4') is-invalid @enderror"
                                            id="photo_4" type="file" name="photo_4">
                                        @error('photo_4')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="photo_5" class="form-label">Photo 5</small>
                                        <input
                                            class="form-control form-control-sm @error('photo_5') is-invalid @enderror"
                                            id="photo_5" type="file" name="photo_5">
                                        @error('photo_5')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="photo_6" class="form-label">Photo 6</small>
                                        <input
                                            class="form-control form-control-sm @error('photo_6') is-invalid @enderror"
                                            id="photo_6" type="file" name="photo_6">
                                        @error('photo_6')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="photo_7" class="form-label">Photo 7</small>
                                        <input
                                            class="form-control form-control-sm @error('photo_7') is-invalid @enderror"
                                            id="photo_7" type="file" name="photo_7">
                                        @error('photo_7')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="photo_8" class="form-label">Photo 8</small>
                                        <input
                                            class="form-control form-control-sm @error('photo_8') is-invalid @enderror"
                                            id="photo_8" type="file" name="photo_8">
                                        @error('photo_8')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="photo_9" class="form-label">Photo 9</small>
                                        <input
                                            class="form-control form-control-sm @error('photo_9') is-invalid @enderror"
                                            id="photo_9" type="file" name="photo_9">
                                        @error('photo_9')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="card p-2 mb-3">
                                    <div class="mb-3">
                                        <small for="photo_10" class="form-label">Photo 10</small>
                                        <input
                                            class="form-control form-control-sm @error('photo_10') is-invalid @enderror"
                                            id="photo_10" type="file" name="photo_10">
                                        @error('photo_10')
                                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary mt-2 mb-4">Submit</button>
                    </div>
                    @endif
                </div>

            </div>
        </div>
    </form>








    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>

    {{-- Jquery --}}
    <script>
        $('#paket_wedding').change(function(){
              var paket = $(this).val();
              console.log(paket)
            $('#demoLabel').text(paket)
            $.ajax({
                type : 'GET',
                dataType : 'JSON',
                url : '/form-guest',
                data :{
                    paket : paket
                },
                success : function(result){
                    console.log(result)
                    $('#demo').empty();
                    $('#demo').append(`
                        <option selected disabled>Silahkan pilih demo</option>
                    `)
                    $.each(result, function(index, element){
                    $('#demo').append(`
                        <option value="`+ element +`" id="demoOption">`+ element +`</option>
                    `)
                    })
                }
            })
          });
    </script>

    <script>
        function imagePreview(){
                const image = document.querySelector('#gambar_pria')
                const imgPreview = document.querySelector('.gambar_pria_preview')

                imgPreview.style.display = 'block';

                const oFReader = new FileReader();
                oFReader.readAsDataURL(image.files[0]);

                oFReader.onload = function(oFREvent){
                    imgPreview.src = oFREvent.target.result;
                }
            }

            function imagePreviewWanita(){
                const image = document.querySelector('#gambar_wanita')
                const imgPreview = document.querySelector('.gambar_wanita_preview')

                imgPreview.style.display = 'block';

                const oFReader = new FileReader();
                oFReader.readAsDataURL(image.files[0]);

                oFReader.onload = function(oFREvent){
                    imgPreview.src = oFREvent.target.result;
                }
            }
    </script>
</body>

</html>
