<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Customer;
use App\Models\PaketWedding;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // User::create([
        //     'name' => 'Admin',
        //     'email' => 'admin@gmail.com',
        //     'role' => 'admin',
        //     'email_verified_at' => '2021-09-28 11:05:24',
        //     'landing_page' => 'https://inv.co.id',
        //     'logo' => 'img/rekanan/invlogo.png',
        //     'password' => Hash::make('asdasdasd')
        // ]);

        for ($i = 1; $i < 51; $i++) {
            PaketWedding::create([
                'jenis_paket' => 'basic',
                'demo_template' => 'demo ' . $i
            ]);
        }

        for ($i = 1; $i < 51; $i++) {
            PaketWedding::create([
                'jenis_paket' => 'premium',
                'demo_template' => 'demo ' . $i
            ]);
        }

        for ($i = 51; $i < 58; $i++) {
            PaketWedding::create([
                'jenis_paket' => 'premium',
                'demo_template' => 'premium ' . $i
            ]);
        }

        for ($i = 51; $i < 58; $i++) {
            PaketWedding::create([
                'jenis_paket' => 'VIP',
                'demo_template' => 'premium ' . $i
            ]);
        }
    }
}
